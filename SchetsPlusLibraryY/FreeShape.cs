﻿using SchetsPlusLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public class FreeShape : Shape
    {
        protected override FSize Size {
            get
            {
                if (PointList == null) return new FSize(0,0);
                float minX = -1;
                float maxX = 1000;
                float minY = -1;
                float maxY = 1000;
                foreach(FPoint p in PointList)
                {
                    if (minX > p.X) minX = p.X;
                    if (maxX < p.X) maxX = p.X;
                    if (minY > p.Y) minX = p.Y;
                    if (maxY < p.Y) maxX = p.Y;
                }
                return new FSize(Math.Abs(maxX - minX), Math.Abs(maxY - minY));

            }
        }

        public List<FPoint> PointList;
        protected const bool closedShape = false;

        public override float X { get { if (PointList.Count > 0) { return PointList.Select(item => item.X).Min(); } else return 0; } set { float x = X; foreach (FPoint p in PointList) { p.X += value - x; } } }
        public override float Y { get { if (PointList.Count > 0) { return PointList.Select(item => item.Y).Min(); } else return 0; } set { float y = Y; foreach (FPoint p in PointList) { p.Y += value - y; } } }
        public override float Width { get { if (PointList.Count > 0) { return PointList.Select(item => item.X).Max() - X; } else return 0; } set { } }
        public override float Height { get { if (PointList.Count > 0) { return PointList.Select(item => item.Y).Max() - Y; } else return 0; } set { } }


        public FreeShape(float x, float y, float w, List<FPoint> PointList) : base(x, y)
        {
            this.StrokeWidth = w;
            this.PointList = PointList;
        }

        public override string Export()
        {
            return $"<rect x=\"{Location.X}\" y=\"{ Location.Y}\" style=\"fill:{colorToString(Fill)};stroke-width:{StrokeWidth};stroke:{colorToString(Stroke)}\" />";
        }

        public override void Draw(Graphics gr)
        {
            Brush br = new SolidBrush(Fill);
            Color alphaStroke = Color.FromArgb((int)(StrokeOpacity * 255), Stroke);
            Pen pen = new Pen(Selected ? Color.Blue : alphaStroke, StrokeWidth)
            {
                StartCap = LineCap.Round,
                EndCap = LineCap.Round
            };
            FPoint previousPoint = null;
            foreach(FPoint p in PointList)
            {
                if(previousPoint != null)
                {
                    gr.DrawLine(pen, previousPoint.X, previousPoint.Y, p.X, p.Y);
                }
                else
                {
                    gr.DrawLine(pen, p.X, p.Y, p.X, p.Y);
                }
                previousPoint = p;
            }
            if (closedShape) gr.DrawLine(pen, PointList.First<FPoint>().X, PointList.First<FPoint>().Y, PointList.Last<FPoint>().X, PointList.Last<FPoint>().X);
        }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            FreeShape output = new FreeShape(X + dx, Y + dy, Width, PointList)
            {
                Fill = Fill,
                FillOpacity = FillOpacity,
                Stroke = Stroke,
                StrokeOpacity = StrokeOpacity,
                StrokeWidth = StrokeWidth,
                StrokeDashes = StrokeDashes,
                Depth = Depth,
                ViewBoxOffset = ViewBoxOffset,
                ZoomFactor = ZoomFactor
            };

            return output;
        }

        public String PointsToString(List<FPoint> list)
        {
            String output = "";

            foreach(FPoint f in list)
            {
                output += $"{f.X},{f.Y} ";
            }

            return output.Remove(output.Length-1);
        }
    }
}
