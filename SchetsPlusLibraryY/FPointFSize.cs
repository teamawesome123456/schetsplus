﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public class FPoint
    {
        public FPoint(float x, float y)
        {
            X = x;
            Y = y;
        }
        public FPoint Copy()
        {
            return new FPoint(X, Y);
        }
        public float X { get; set; }
        public float Y { get; set; }
        public override string ToString()
        {
            return $"FPoint {X} {Y}";
        }
        public PointF ToPointF()
        {
            return new PointF(X, Y);
        }
        public Point ToPoint()
        {
            return new Point((int)X, (int)Y);
        }
    }

    public class FSize
    {
        public FSize(float width, float height)
        {
            Width = width;
            Height = height;
        }

        public float Width { get; set; }
        public float Height { get; set; }

        public static implicit operator FSize(Size s) { return new FSize(s.Width, s.Height); }
        public static FSize operator* (FSize s, float f)
        {
            return new FSize(s.Width * f, s.Height * f);
        }
        public override string ToString()
        {
            return $"FSize {Width} {Height}";
        }
    }
}
