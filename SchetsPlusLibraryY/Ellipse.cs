﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public class Ellipse : Shape
    {
        public Ellipse(float x, float y, float w, float h) : base(x, y)
        {
            this.Size = new FSize(w, h);
        }

        public override string Export()
        {
            float x = X;
            float y = Y;
            float w = Math.Abs(Width)*0.5f;
            float h = Math.Abs(Height)*0.5f;

            if (Width < 0) x += Width;
            if (Height < 0) y += Height;

            x += w;
            y += h;

            string output = "";

            if (w != h)
            {
                output = "<ellipse ";
                if (x != 0) output += makeAttribute("cx", x);
                if (y != 0) output += makeAttribute("cy", y);
                if (w != 0) output += makeAttribute("rx", w);
                if (h != 0) output += makeAttribute("ry", h);
            }
            else
            {
                output = "<circle ";
                if (x != 0) output += makeAttribute("cx", x);
                if (y != 0) output += makeAttribute("cy", y);
                if (w != 0) output += makeAttribute("r", w);
            }
            output += createExportStyle();
            output += createExportTransform();
            output += "/>";
            return output;
        }

        public override void Draw(Graphics gr)
        {
            Brush br = new SolidBrush(Fill);
            Pen pen = new Pen(Stroke, StrokeWidth);
            if (StrokeDashes != null) pen.DashPattern = StrokeDashes;

            float x = X;
            float y = Y;

            if (Width < 0) x += Width;
            if (Height < 0) y += Height;

            FPoint loc = translatePoint(x, y);
            FSize size = translateSize(Math.Abs(Size.Width), Math.Abs(Size.Height));

            Matrix m = new Matrix();
            m.RotateAt(Angle, RotatePoint);
            gr.Transform = m;

            gr.FillEllipse(br, loc.X, loc.Y, size.Width, size.Height);
            if (StrokeWidth > 0) gr.DrawEllipse(pen, loc.X, loc.Y, size.Width, size.Height);
            gr.ResetTransform();
            if (Selected) DrawSelected(gr);
        }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            Ellipse output = new Ellipse(X + dx, Y + dy, Width, Height)
            {
                Fill = Fill,
                FillOpacity = FillOpacity,
                Stroke = Stroke,
                StrokeOpacity = StrokeOpacity,
                StrokeWidth = StrokeWidth,
                StrokeDashes = StrokeDashes,
                Depth = Depth,
                ViewBoxOffset = ViewBoxOffset,
                ZoomFactor = ZoomFactor,
                Angle = Angle
            };

            return output;
        }
    }
}
