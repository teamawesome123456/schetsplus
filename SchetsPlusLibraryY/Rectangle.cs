﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public class Rect : Shape
    {
        private bool translate = true;

        public Rect(float x, float y, float w, float h, bool translate = true) : base(x, y)
        {
            this.Size = new FSize(w, h);
            this.translate = translate;
        }

        public override string Export()
        {
            //Simplify
            float x = X;
            float y = Y;
            float w = Math.Abs(Size.Width);
            float h = Math.Abs(Size.Height);

            if (Size.Width < 0) x += Size.Width;
            if (Size.Height < 0) y += Size.Height;

            //Create the string
            string output = "<rect ";
            if (x != 0) output += makeAttribute("x", x);
            if (y != 0) output += makeAttribute("y", y);
            if (w != 0) output += makeAttribute("width", w);
            if (h != 0) output += makeAttribute("height", h);
            output += createExportStyle();
            output += createExportTransform();
            output += "/>";
            return output;
        }

        public override void Draw(Graphics gr)
        {
            float x = X;
            float y = Y;

            if (Size.Width < 0) x += Size.Width;
            if (Size.Height < 0) y += Size.Height;

            FPoint loc;
            FSize size;
            float sw;

            if (translate)
            {
                loc = translatePoint(x, y);
                size = translateSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = translateStrokeWidth(StrokeWidth);
            }
            else
            {
                loc = new FPoint(x, y);
                size = new FSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = StrokeWidth;
            }

            Matrix m = new Matrix();
            m.RotateAt(Angle, RotatePoint);
            gr.Transform = m;

            Brush br = new SolidBrush(Fill);
            Pen pen = new Pen(Stroke, sw);
            if (StrokeDashes != null) pen.DashPattern = StrokeDashes;

            gr.FillRectangle(br, loc.X, loc.Y, size.Width, size.Height);
            if (StrokeWidth > 0) gr.DrawRectangle(pen, loc.X, loc.Y, size.Width, size.Height);

            gr.ResetTransform();

            if (Selected) DrawSelected(gr);
            
        }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            Rect output = new Rect(X + dx, Y + dy, Width, Height)
            {
                Fill = Fill,
                FillOpacity = FillOpacity,
                Stroke = Stroke,
                StrokeOpacity = StrokeOpacity,
                StrokeWidth = StrokeWidth,
                StrokeDashes = StrokeDashes,
                Depth = Depth,
                ViewBoxOffset = ViewBoxOffset,
                ZoomFactor = ZoomFactor,
                Angle = Angle
            };

            return output;
        }
    }
}
