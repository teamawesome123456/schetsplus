﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchetsPlusLibrary
{
    public class Text : Shape
    {
        public string Content { get; set; }
        public Font Font { get; set; }

        public override float Width
        {
            get
            {
                var gp = new GraphicsPath();
                gp.AddString(Content, Font.FontFamily, (int)Font.Style, Font.Size, new PointF(X, Y), new StringFormat());
                return gp.GetBounds().Width + StrokeWidth*2;
            }
            set { base.Width = value; }
        }
        public override float Height
        {
            get
            {
                var gp = new GraphicsPath();
                gp.AddString(Content, Font.FontFamily, (int)Font.Style, Font.Size,new PointF(X, Y), new StringFormat());
                return gp.GetBounds().Height + StrokeWidth * 2;
            }
            set { base.Height = value; }
        }

        public Text(float x, float y, string text = "No text yet", Font font = null) : base(x, y)
        {
            Fill = Color.Black;
            Content = text;
            if (font != null) Font = font;
            else Font = new Font("Arial", 12f);
        }

        public override void Draw(Graphics gr)
        {
            try
            {
                Matrix m = new Matrix();
                m.RotateAt(Angle, RotatePoint);
                gr.Transform = m;
            }
            catch { }

            var gp = new GraphicsPath();
            gp.AddString(Content, Font.FontFamily, (int)Font.Style, Font.Size / ZoomFactor, translatePoint(X, Y).ToPointF(), new StringFormat());
            gr.FillPath(new SolidBrush(Fill), gp);
            if (StrokeWidth != 0) gr.DrawPath(new Pen(Stroke, translateStrokeWidth(StrokeWidth)), gp);
            gr.ResetTransform();
            if (Selected) DrawSelected(gr);
        }

        public override string Export()
        {
            string output = "";

            output = "<text ";
            if (X != 0) output += makeAttribute("x", X);
            if (Y != 0) output += makeAttribute("y", Y);

            output += createExportStyle() + " ";
            output += createExportTransform();
            output += ">" + Environment.NewLine;
            output += Content + Environment.NewLine;
            output += "</text>";
            return output;
        }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            Text output = new Text(X + dx, Y + dy)
            {
                Fill = Fill,
                FillOpacity = FillOpacity,
                Stroke = Stroke,
                StrokeOpacity = StrokeOpacity,
                StrokeWidth = StrokeWidth,
                StrokeDashes = StrokeDashes,
                Depth = Depth,
                ViewBoxOffset = ViewBoxOffset,
                ZoomFactor = ZoomFactor,
                Angle = Angle,
                Font = Font,
                Content = Content
            };

            return output;
        }

        protected override string createExportStyle()
        {
            string output = base.createExportStyle();
            output = output.Substring(0, output.Length - 2);
            output += makeStyle("font-family", Font.Name);
            output += makeStyle("font-size", Font.Size);
            if (Font.Underline) output += makeStyle("text-decoration", "underline");
            else if (Font.Strikeout) output += makeStyle("text-decoration", "line-through"); //SVG can only handle one line at a time
            if (Font.Bold) output += makeStyle("font-weight", "bold");
            if (Font.Italic) output += makeStyle("font-style", "italic");
            output += "\"";
            return output;
        }
    }
}
