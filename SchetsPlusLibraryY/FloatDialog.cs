﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchetsPlusLibrary
{
    public class FloatDialog
    {
        public static float ShowDialog(string caption, string defaultValue)
        {
            Form prompt = new Form
            {
                ClientSize = new System.Drawing.Size(100, 60),
                Text = caption,
                MinimizeBox = false,
                MaximizeBox = false,
                FormBorderStyle = FormBorderStyle.FixedDialog
            };
            TextBox inputBox = new TextBox() { Text = defaultValue, Top = 10, Width=100};
            Button confirmation = new Button() { Text = "Ok", Left = 60, Width = 40, Top = 40 };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            inputBox.KeyUp += (o, i) => { if (i.KeyCode == Keys.Enter) { prompt.Close(); }; };
            

            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(inputBox);
            confirmation.Focus();
            inputBox.Select();
            prompt.ShowDialog();
            
            prompt.AcceptButton = confirmation;
            prompt.ActiveControl = inputBox;

            try
            {
                return (float)Convert.ToDouble(inputBox.Text);
            }
            catch
            {
                var msg = MessageBox.Show("The number you entered was not parseble (rememder to use dots instead of commas).");
                return -1;
            }
        }
    }
}
