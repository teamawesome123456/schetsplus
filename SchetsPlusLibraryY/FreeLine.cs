﻿using SchetsPlusLibrary;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;

namespace SchetsPlusLibrary
{
    public class FreeLine : FreeShape
    {
        private new const bool closedShape = false;
        bool translate;

        public FreeLine(float x, float y, float w, List<FPoint> PointList, bool translate = true) : base(x, y, w, PointList)
        {
            this.translate = translate;
        }

        public override void Draw(Graphics gr)
        {
            FPoint loc;
            List<FPoint> translatedPointList = new List<FPoint>();
            FSize size;
            float sw;

            if (translate)
            {
                loc = translatePoint(X, Y);

                foreach (FPoint p in PointList)
                {
                    translatedPointList.Add(translatePoint(p));
                }

                size = translateSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = translateStrokeWidth(StrokeWidth);
            }
            else
            {
                loc = new FPoint(X, Y);
                size = new FSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = StrokeWidth;
            }

            Brush br = new SolidBrush(Fill);
            Color alphaStroke = Color.FromArgb(StrokeOpacity, Stroke);
            Pen pen = new Pen(Selected ? Color.Blue : alphaStroke, sw) { StartCap = LineCap.Round, EndCap = LineCap.Round };
            FPoint previousPoint = null;

            foreach (FPoint p in translatedPointList)
            {
                if (previousPoint != null)
                {
                    gr.DrawLine(pen, previousPoint.X, previousPoint.Y, p.X, p.Y);
                }
                else
                {
                    gr.DrawLine(pen, p.X, p.Y, p.X, p.Y);
                }
                previousPoint = p;
            }
            if (Selected) DrawSelected(gr);
        }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            FreeLine output = new FreeLine(X + dx, Y + dy, Width, PointList)
            {
                Fill = Fill,
                FillOpacity = FillOpacity,
                Stroke = Stroke,
                StrokeOpacity = StrokeOpacity,
                StrokeWidth = StrokeWidth,
                StrokeDashes = StrokeDashes,
                Depth = Depth,
                ViewBoxOffset = ViewBoxOffset,
                ZoomFactor = ZoomFactor
            };

            return output;
        }
        public override string Export()
        {
            //Create the string
            string output = "<polyline ";
            output += makeAttribute("points", PointsToString(PointList));
            output += createExportStyle();
            output += "/>";
            return output;
            //return $"<polyline fill=\"none\" stroke=\"{colorToString(Stroke)}\" stroke-width=\"{StrokeWidth}\" opacity=\"{StrokeOpacity}\" =\"{PointsToString(PointList)}\"/>";
        }
    }
}
