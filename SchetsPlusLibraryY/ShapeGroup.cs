﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public class ShapeGroup : Shape
    {
        private List<Shape> items = new List<Shape>();
        private bool document;

        public ShapeGroup(Shape[] itemsIn = null, bool document = true, float x = 0, float y = 0) : base(x, y)
        {
            this.Location.X = x;
            this.Location.Y = y;
            this.Size.Width = 0;
            this.Size.Height = 0;
            this.document = document;
            if (itemsIn != null) this.items = new List<Shape>(itemsIn);
        }

        public override void Draw(Graphics gr)
        {
            foreach (Shape s in items)
            {
                s.Draw(gr);
            }

            FPoint loc = translatePoint(X, Y);
            FSize size = translateSize(Width, Height);

            if (Selected) DrawSelected(gr);
        }

        public override string Export()
        {
            string output = "<g>" + Environment.NewLine;
            foreach (Shape s in items)
            {
                output += s.Export();
                output += Environment.NewLine;
            }
            output += "</g>";
            return output;
        }

        public void AddItem(Shape s)
        {
            items.Add(s);
            if (document) { s.ShowSelected = false; s.Depth = this.Depth + s.Depth; }
        }

        public void AddItems(Shape[] s)
        {
            items.Concat(s);
            if (document)
            {
                foreach (Shape xs in s)
                {
                    xs.ShowSelected = false;
                    xs.Depth = this.Depth + xs.Depth;
                }
            }
        }

        public override float X
        {
            get
            {
                try { return items.Select(item => item.X).Min(); }
                catch { return 0; }
            }
            set
            {
                foreach (Shape s in items)
                {
                    s.X += (value - Location.X);
                }
                Location.X = value;
            }
        }
        public override float Y
        {
            get
            {
                try { return items.Select(item => item.Y).Min(); }
                catch { return 0; }
            }
            set
            {
                foreach (Shape s in items)
                {
                    s.Y += (value - Location.Y);
                }
                Location.Y = value;
            }
        }
        public override float Width
        {
            get
            {
                try { return items.Select(item => item.X + item.Width).Max() - Location.X; }
                catch { return 0; }
            }
            set
            {
                foreach (Shape s in items)
                {
                    s.Width *= value / Size.Width;
                    s.X = (s.X - Location.X) * value / Size.Width + Location.X;
                }
                Size.Width = value;
            }
        }
        public override float Height
        {
            get
            {
                try { return items.Select(item => item.Y + item.Height).Max() - Location.Y; }
                catch { return 0; }
            }
            set
            {
                foreach (Shape s in items)
                {
                    s.Height *= value / Size.Height;
                    s.Y = (s.Y - Location.Y) * value / Size.Height + Location.Y;
                }
                Size.Height = value;
            }
        }

        public override Color Fill { get { return Color.FromArgb(0, 0, 0, 0); } set { foreach (Shape item in items) { item.Fill = value; } } }
        public override Color Stroke { get { return Color.FromArgb(0, 0, 0, 0); } set { foreach (Shape item in items) { item.Stroke = value; } } }
        public override float StrokeWidth { get { return 1; } set { foreach (Shape item in items) { item.StrokeWidth = value; } } }
        public override float[] StrokeDashes { get { return new float[] { 1 }; } set { foreach (Shape item in items) { item.StrokeDashes = value; } } }
        public override int StrokeOpacity { get { return -1; } set { foreach (Shape item in items) { item.StrokeOpacity = value; } } }
        public override int FillOpacity { get { return -1; } set { foreach (Shape item in items) { item.FillOpacity = value; } } }
        public override FPoint ViewBoxOffset { get { return viewBoxOffset; } set { foreach (Shape item in items) { item.ViewBoxOffset = value; } viewBoxOffset = value; } }
        private FPoint viewBoxOffset;
        public override float ZoomFactor { get { return zoomFactor; } set { foreach (Shape item in items) { item.ZoomFactor = value; } zoomFactor = value; } }
        private float zoomFactor;
        public override bool Selected { get { return selected; } set { foreach (Shape item in items) { item.Selected = false; } selected = value; } }
        private bool selected = false;
        public int Items { get { return items.Count; } }

        public override Shape Clone(float dx = 0, float dy = 0)
        {
            List<Shape> newItemsList = new List<Shape>();

            foreach (Shape s in items)
            {
                newItemsList.Add(s.Clone(dx, dy));
            }

            ShapeGroup output = new ShapeGroup(newItemsList.ToArray())
            {
                ZoomFactor = zoomFactor,
                ViewBoxOffset = viewBoxOffset
            };

            return output;
        }

        public Shape[] DeGroup()
        {
            List<Shape> output = new List<Shape>();

            for (int i = 0; i < items.Count; i++)
            {
                output.Add(items[i]);
                items.Remove(items[i]);
                i--;
            }

            return output.ToArray();
        }
    }
}
