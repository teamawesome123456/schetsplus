﻿using SchetsPlusLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{

    public class Curve : FreeLine
    {
        public float[] ShapingRectangles
        {
            get
            {
                float[] output = new float[PointList.Count * 2];
                for (int i = 0; i < PointList.Count * 2 - 1; i += 2)
                {
                    output[i] = PointList.ElementAt(i /2).X;
                    output[i+1] = PointList.ElementAt(i /2).Y;
                }
                float d = 3 + 0.5f * translateStrokeWidth(StrokeWidth) + 5;
                return output;
            }
        }
        bool translate;

        public Curve(float x, float y, float w, List<FPoint> PointList, bool translate = true) : base(x, y, w, PointList, translate)
        {
            this.translate = translate;
        }

        public override void Draw(Graphics gr)
        {
            List<FPoint> translatedPointList = new List<FPoint>();
            FSize size;
            float sw;

            if (translate)
            {
                foreach (FPoint p in PointList)
                {
                    translatedPointList.Add(translatePoint(p));
                }

                size = translateSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = translateStrokeWidth(StrokeWidth);
            }
            else
            {
                size = new FSize(Math.Abs(Size.Width), Math.Abs(Size.Height));
                sw = StrokeWidth;
            }
            Brush br = new SolidBrush(Fill);
            Color alphaStroke = Color.FromArgb((int)(StrokeOpacity), Stroke);
            Pen pen = new Pen(Selected ? Color.Blue : alphaStroke, sw)
            {
                StartCap = LineCap.Round,
                EndCap = LineCap.Round
            };
            List<PointF> PointFList = new List<PointF>();

            for (int i = 1; i <= translatedPointList.Count; i++)
            {
                if (i == translatedPointList.Count && (i - 1) % 3 != 0)
                {
                    for (int j = 0; j < 3 - ((i - 1) % 3); j++)
                    {
                        PointFList.Add(translatedPointList.ElementAt((int)(3 * (Math.Floor((double)(i - 1) / 3.0)))).ToPointF());
                    }
                }
                PointFList.Add(translatedPointList.ElementAt(i - 1).ToPointF());
            }
            PointF[] PointFArray = PointFList.ToArray();
            gr.DrawBeziers(pen, PointFArray);
            if (closedShape) gr.DrawLine(pen, PointList.First<FPoint>().X, PointList.First<FPoint>().Y, PointList.Last<FPoint>().X, PointList.Last<FPoint>().X);
            if (Selected) DrawSelected(gr);
        }

        protected override void DrawSelected(Graphics gr)
        {
            Pen pen = new Pen(Color.Black);
            float d = 3 + 0.5f * translateStrokeWidth(StrokeWidth);
            FPoint loc = translatePoint(X - d, Y - d);
            FPoint lastloc = translatePoint(X - d, Y - d);
            FSize size = translateSize(Width + 2 * d, Height + 2 * d);

            for (int i = 0; i < ShapingRectangles.Length; i += 2)
            {
                loc = translatePoint(ShapingRectangles[i], ShapingRectangles[i + 1]);
                size = translateSize(ResizeRectangleSize, ResizeRectangleSize);
                try
                {
                    if ((i-2) % 3 == 0 || (i-2) % 3 == 1)
                    {
                        float rw = size.Width * 0.5f;
                        gr.DrawLine(pen, loc.X+rw, loc.Y+rw, lastloc.X+rw, lastloc.Y+rw);
                    }
                }
                catch { }
                gr.DrawRectangle(pen, loc.X, loc.Y, size.Width, size.Height);
                gr.FillRectangle(new SolidBrush(i % 3 == 0 ? Color.White : Color.Black), loc.X, loc.Y, size.Width, size.Height);
                lastloc = loc;
            }
        }

        public override string Export()
        {
            string output = $"<path d=\"{bezierToString(PointList.ToArray())}\" ";
            output += createExportStyle();
            output += "/>";

            return output;
        }

        private string bezierToString(FPoint[] array)
        {
            string output = "M";

            for (int i = 0; i < array.Length; i++)
            {
                output += array[i].X + " " + array[i].Y;
                if (i + 1 != array.Length)
                {
                    if ((i) % 3 == 0) output += " C";
                    else output += ", ";
                }
            }

            return output;
        }
    }
}
