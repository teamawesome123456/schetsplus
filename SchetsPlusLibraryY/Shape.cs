﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchetsPlusLibrary
{
    public abstract class Shape
    {
        public Shape(float x, float y)
        {
            this.Location = new FPoint(x, y);
            this.Size = new FSize(0, 0);
            Fill = Color.White;
            Stroke = Color.Black;
            FillOpacity = 255;
            StrokeOpacity = 255;
            StrokeWidth = 1;
            StrokeDashes = null;
            Depth = 0;
            Selected = false;
            ShowSelected = false;
            ViewBoxOffset = new FPoint(0, 0);
        }

        protected FPoint Location { get; set; }
        protected virtual FSize Size { get; set; }

        public virtual float X { get { return Location.X; } set { Location = new FPoint(value, Y); } }
        public virtual float Y { get { return Location.Y; } set { Location = new FPoint(X, value); } }
        public virtual float Width { get { return Size.Width; } set { Size = new FSize(value, Height); } }
        public virtual float Height { get { return Size.Height; } set { Size = new FSize(Width, value); } }

        private float angle = 0;
        public float Angle { get { return angle; } set { angle = value % 360; } }

        private Color fill;
        public virtual Color Fill { get { return Color.FromArgb(FillOpacity, fill.R, fill.G, fill.B); } set { fill = value; } }
        private Color stroke;
        public virtual Color Stroke { get { return Color.FromArgb(StrokeOpacity, stroke.R, stroke.G, stroke.B); } set { stroke = value; } }
        public virtual float StrokeWidth { get; set; }
        public virtual float[] StrokeDashes { get; set; }
        public virtual int Depth { get; set; }
        public virtual int FillOpacity { get; set; }
        public virtual int StrokeOpacity { get; set; }
        public virtual bool Selected { get; set; }
        public virtual bool ShowSelected { get; set; }
        public abstract string Export();
        public virtual FPoint ViewBoxOffset { get; set; }
        public virtual float ZoomFactor { get; set; }
        public abstract void Draw(Graphics gr);

        public float[] ResizeRectangles
        {
            get
            {
                float d = 3 + 0.5f * translateStrokeWidth(StrokeWidth) + 5;
                float wd = ResizeRectangleSize + d;
                return new float[] { X - wd, Y - wd, X + 0.5f * Width,
                    Y - wd, X + Width + d, Y - wd,X - wd, Y + 0.5f*Height,
                    X + Width + d, Y + 0.5f * Height,  X - wd, Y + Height + d,
                    X + 0.5f * Width, Y + Height + d, X + Width + d, Y + Height + d };
            }
        }

        public float ResizeRectangleSize { get { return 7 * ZoomFactor; } }

        protected string colorToString(Color c)
        {
            return $"rgb({c.R},{c.G},{c.B})";
        }

        protected virtual void DrawSelected(Graphics gr)
        {
            Pen pen = new Pen(Color.Black, 1) { DashPattern = new float[] { 5, 5 } };
            float d = 3 + 0.5f * translateStrokeWidth(StrokeWidth);

            FPoint loc = translatePoint(X - d, Y - d);
            FSize size = translateSize(Width + 2 * d, Height + 2 * d);
            gr.DrawRectangle(pen, loc.X, loc.Y, size.Width, size.Height);

            Pen pen2 = new Pen(Color.Black);
            for (int i = 0; i < ResizeRectangles.Length; i += 2)
            {
                loc = translatePoint(ResizeRectangles[i], ResizeRectangles[i + 1]);
                size = translateSize(ResizeRectangleSize, ResizeRectangleSize);
                gr.DrawRectangle(pen2, loc.X, loc.Y, size.Width, size.Height);
            }
        }

        public static int Compare(Shape x, Shape y)
        {
            if (x.Depth > y.Depth) return -1;
            if (x.Depth < y.Depth) return 1;
            return 0;
        }

        public void Simplify()
        {
            float x = X;
            float y = Y;
            float w = Math.Abs(Width);
            float h = Math.Abs(Height);

            if (Width < 0) x += Width;
            if (Height < 0) y += Height;

            Location = new FPoint(x, y);
            Size = new FSize(w, h);
        }

        public abstract Shape Clone(float dx = 0, float dy = 0);

        protected FPoint translatePoint(float x, float y) { return translatePoint(new FPoint(x, y)); }

        protected FPoint translatePoint(FPoint p)
        {
            return new FPoint((p.X - ViewBoxOffset.X) / ZoomFactor, (p.Y - ViewBoxOffset.Y) / ZoomFactor);
        }

        protected FSize translateSize(float w, float h) { return translateSize(new FSize(w, h)); }

        protected FSize translateSize(FSize s)
        {
            return new FSize(s.Width / ZoomFactor, s.Height / ZoomFactor);
        }

        protected float translateStrokeWidth(float strokeWidth)
        {
            return (strokeWidth / ZoomFactor);
        }

        protected string makeStyle(string name, object value)
        {
            return makeAttribute(name, value, ":", ";");
        }

        protected string makeAttribute(string name, object value, string delimeter = "=\"", string end = "\" ")
        {
            string valuestring = "";
            if (value.GetType() == typeof(Color))
            {
                valuestring = colorToString((Color)value);
            }
            else valuestring = value.ToString();
            return name + delimeter + valuestring + end;
        }

        protected virtual string createExportStyle()
        {
            string styles = "";
            styles += makeStyle("fill", Fill);
            if (FillOpacity != 255) styles += makeStyle("fill-opacity", FillOpacity);
            styles += makeStyle("stroke", Stroke);
            if (StrokeOpacity != 255) styles += makeStyle("stroke-opacity", StrokeOpacity);
            if (StrokeWidth != 1) styles += makeStyle("stroke-width", StrokeWidth);
            if (StrokeDashes != null) styles += makeStyle("stroke-dasharray", StrokeDashes);
            return makeAttribute("style", styles);
        }

        protected virtual string createExportTransform()
        {
            string transform = $"rotate({Angle} {RotatePoint.X} {RotatePoint.Y})";
            return makeAttribute("transform", transform);
        }

        public PointF RotatePoint
        {
            get
            {
                FPoint loc = translatePoint(X, Y);
                FSize size = translateSize(Math.Abs(Width), Math.Abs(Height));
                return new PointF(loc.X + 0.5f * size.Width, loc.Y + 0.5f * size.Height);
            }
        }
    }
}
