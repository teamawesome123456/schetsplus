﻿namespace SchetsPlusUI
{
    partial class SchetsPlus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchetsPlus));
            this.toolNone = new System.Windows.Forms.Button();
            this.toolPen = new System.Windows.Forms.Button();
            this.toolCurve = new System.Windows.Forms.Button();
            this.toolRect = new System.Windows.Forms.Button();
            this.selectFill = new System.Windows.Forms.Button();
            this.selectStroke = new System.Windows.Forms.Button();
            this.selectStrokeWidth = new System.Windows.Forms.Button();
            this.selectDepth = new System.Windows.Forms.Button();
            this.TopUIPanel = new System.Windows.Forms.Panel();
            this.toolText = new System.Windows.Forms.Button();
            this.toolEllipse = new System.Windows.Forms.Button();
            this.MainDrawingPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BottomUIPanel = new System.Windows.Forms.Panel();
            this.selectStrokeOpacityValue = new System.Windows.Forms.Label();
            this.selectStrokeOpacity = new System.Windows.Forms.Button();
            this.selectFillOpacityValue = new System.Windows.Forms.Label();
            this.selectFillOpacity = new System.Windows.Forms.Button();
            this.selectDeleteItem = new System.Windows.Forms.Button();
            this.selectStrokeValue = new System.Windows.Forms.Button();
            this.selectFillValue = new System.Windows.Forms.Button();
            this.selectStrokeWidthValue = new System.Windows.Forms.Label();
            this.selectDepthValue = new System.Windows.Forms.Label();
            this.SideUIPanel = new System.Windows.Forms.Panel();
            this.clearDrawing = new System.Windows.Forms.Button();
            this.saveAsButton = new System.Windows.Forms.Button();
            this.openSVGButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.exportAsBitmapButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.SideUIPanelTwo = new System.Windows.Forms.Panel();
            this.itemAngle = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.FontUIPanel = new System.Windows.Forms.Panel();
            this.itemText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.itemFontName = new System.Windows.Forms.Label();
            this.itemFontButton = new System.Windows.Forms.Button();
            this.itemFontSize = new System.Windows.Forms.Label();
            this.itemHeight = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.itemWidth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.itemY = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.itemX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.invisibleButton = new System.Windows.Forms.Button();
            this.previewPanel = new SchetsPlusUI.TransparentPanel();
            this.TopUIPanel.SuspendLayout();
            this.MainDrawingPanel.SuspendLayout();
            this.BottomUIPanel.SuspendLayout();
            this.SideUIPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SideUIPanelTwo.SuspendLayout();
            this.FontUIPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolNone
            // 
            this.toolNone.Location = new System.Drawing.Point(3, 3);
            this.toolNone.Name = "toolNone";
            this.toolNone.Size = new System.Drawing.Size(75, 23);
            this.toolNone.TabIndex = 0;
            this.toolNone.Text = "None";
            this.toolNone.UseVisualStyleBackColor = true;
            // 
            // toolPen
            // 
            this.toolPen.Location = new System.Drawing.Point(81, 3);
            this.toolPen.Name = "toolPen";
            this.toolPen.Size = new System.Drawing.Size(75, 23);
            this.toolPen.TabIndex = 1;
            this.toolPen.Text = "Pen";
            this.toolPen.UseVisualStyleBackColor = true;
            // 
            // toolCurve
            // 
            this.toolCurve.Location = new System.Drawing.Point(162, 3);
            this.toolCurve.Name = "toolCurve";
            this.toolCurve.Size = new System.Drawing.Size(75, 23);
            this.toolCurve.TabIndex = 2;
            this.toolCurve.Text = "Curve";
            this.toolCurve.UseVisualStyleBackColor = true;
            // 
            // toolRect
            // 
            this.toolRect.Location = new System.Drawing.Point(243, 3);
            this.toolRect.Name = "toolRect";
            this.toolRect.Size = new System.Drawing.Size(75, 23);
            this.toolRect.TabIndex = 3;
            this.toolRect.Text = "Rectangle";
            this.toolRect.UseVisualStyleBackColor = true;
            // 
            // selectFill
            // 
            this.selectFill.Location = new System.Drawing.Point(3, 3);
            this.selectFill.Name = "selectFill";
            this.selectFill.Size = new System.Drawing.Size(27, 23);
            this.selectFill.TabIndex = 4;
            this.selectFill.Text = "Fill";
            this.selectFill.UseVisualStyleBackColor = true;
            // 
            // selectStroke
            // 
            this.selectStroke.Location = new System.Drawing.Point(155, 3);
            this.selectStroke.Name = "selectStroke";
            this.selectStroke.Size = new System.Drawing.Size(48, 23);
            this.selectStroke.TabIndex = 5;
            this.selectStroke.Text = "Stroke";
            this.selectStroke.UseVisualStyleBackColor = true;
            // 
            // selectStrokeWidth
            // 
            this.selectStrokeWidth.Location = new System.Drawing.Point(321, 3);
            this.selectStrokeWidth.Name = "selectStrokeWidth";
            this.selectStrokeWidth.Size = new System.Drawing.Size(75, 23);
            this.selectStrokeWidth.TabIndex = 6;
            this.selectStrokeWidth.Text = "StrokeWidth";
            this.selectStrokeWidth.UseVisualStyleBackColor = true;
            // 
            // selectDepth
            // 
            this.selectDepth.Location = new System.Drawing.Point(426, 3);
            this.selectDepth.Name = "selectDepth";
            this.selectDepth.Size = new System.Drawing.Size(44, 23);
            this.selectDepth.TabIndex = 7;
            this.selectDepth.Text = "Depth";
            this.selectDepth.UseVisualStyleBackColor = true;
            // 
            // TopUIPanel
            // 
            this.TopUIPanel.Controls.Add(this.toolText);
            this.TopUIPanel.Controls.Add(this.toolEllipse);
            this.TopUIPanel.Controls.Add(this.toolNone);
            this.TopUIPanel.Controls.Add(this.toolPen);
            this.TopUIPanel.Controls.Add(this.toolCurve);
            this.TopUIPanel.Controls.Add(this.toolRect);
            this.TopUIPanel.Location = new System.Drawing.Point(0, 0);
            this.TopUIPanel.Name = "TopUIPanel";
            this.TopUIPanel.Size = new System.Drawing.Size(502, 30);
            this.TopUIPanel.TabIndex = 8;
            // 
            // toolText
            // 
            this.toolText.Location = new System.Drawing.Point(403, 3);
            this.toolText.Name = "toolText";
            this.toolText.Size = new System.Drawing.Size(75, 23);
            this.toolText.TabIndex = 5;
            this.toolText.Text = "Text";
            this.toolText.UseVisualStyleBackColor = true;
            // 
            // toolEllipse
            // 
            this.toolEllipse.Location = new System.Drawing.Point(322, 3);
            this.toolEllipse.Name = "toolEllipse";
            this.toolEllipse.Size = new System.Drawing.Size(75, 23);
            this.toolEllipse.TabIndex = 4;
            this.toolEllipse.Text = "Ellipse";
            this.toolEllipse.UseVisualStyleBackColor = true;
            // 
            // MainDrawingPanel
            // 
            this.MainDrawingPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MainDrawingPanel.Controls.Add(this.previewPanel);
            this.MainDrawingPanel.Controls.Add(this.panel1);
            this.MainDrawingPanel.Location = new System.Drawing.Point(0, 30);
            this.MainDrawingPanel.Name = "MainDrawingPanel";
            this.MainDrawingPanel.Size = new System.Drawing.Size(400, 570);
            this.MainDrawingPanel.TabIndex = 9;
            this.MainDrawingPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.mainPanelPaint);
            this.MainDrawingPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mainPanelMouseDown);
            this.MainDrawingPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mainPanelMouseMove);
            this.MainDrawingPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mainPanelMouseUp);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(84, 581);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(19, 19);
            this.panel1.TabIndex = 10;
            // 
            // BottomUIPanel
            // 
            this.BottomUIPanel.Controls.Add(this.selectStrokeOpacityValue);
            this.BottomUIPanel.Controls.Add(this.selectStrokeOpacity);
            this.BottomUIPanel.Controls.Add(this.selectFillOpacityValue);
            this.BottomUIPanel.Controls.Add(this.selectFillOpacity);
            this.BottomUIPanel.Controls.Add(this.selectDeleteItem);
            this.BottomUIPanel.Controls.Add(this.selectStrokeValue);
            this.BottomUIPanel.Controls.Add(this.selectFillValue);
            this.BottomUIPanel.Controls.Add(this.selectStrokeWidthValue);
            this.BottomUIPanel.Controls.Add(this.selectDepthValue);
            this.BottomUIPanel.Controls.Add(this.selectFill);
            this.BottomUIPanel.Controls.Add(this.selectStroke);
            this.BottomUIPanel.Controls.Add(this.selectStrokeWidth);
            this.BottomUIPanel.Controls.Add(this.selectDepth);
            this.BottomUIPanel.Location = new System.Drawing.Point(0, 603);
            this.BottomUIPanel.Name = "BottomUIPanel";
            this.BottomUIPanel.Size = new System.Drawing.Size(576, 30);
            this.BottomUIPanel.TabIndex = 10;
            // 
            // selectStrokeOpacityValue
            // 
            this.selectStrokeOpacityValue.AutoSize = true;
            this.selectStrokeOpacityValue.Location = new System.Drawing.Point(294, 9);
            this.selectStrokeOpacityValue.Name = "selectStrokeOpacityValue";
            this.selectStrokeOpacityValue.Size = new System.Drawing.Size(13, 13);
            this.selectStrokeOpacityValue.TabIndex = 16;
            this.selectStrokeOpacityValue.Text = "1";
            // 
            // selectStrokeOpacity
            // 
            this.selectStrokeOpacity.Location = new System.Drawing.Point(233, 4);
            this.selectStrokeOpacity.Name = "selectStrokeOpacity";
            this.selectStrokeOpacity.Size = new System.Drawing.Size(57, 23);
            this.selectStrokeOpacity.TabIndex = 15;
            this.selectStrokeOpacity.Text = "Opacity";
            this.selectStrokeOpacity.UseVisualStyleBackColor = true;
            this.selectStrokeOpacity.Click += new System.EventHandler(this.selectStrokeOpacityClick);
            // 
            // selectFillOpacityValue
            // 
            this.selectFillOpacityValue.AutoSize = true;
            this.selectFillOpacityValue.Location = new System.Drawing.Point(125, 8);
            this.selectFillOpacityValue.Name = "selectFillOpacityValue";
            this.selectFillOpacityValue.Size = new System.Drawing.Size(13, 13);
            this.selectFillOpacityValue.TabIndex = 14;
            this.selectFillOpacityValue.Text = "1";
            // 
            // selectFillOpacity
            // 
            this.selectFillOpacity.Location = new System.Drawing.Point(62, 3);
            this.selectFillOpacity.Name = "selectFillOpacity";
            this.selectFillOpacity.Size = new System.Drawing.Size(57, 23);
            this.selectFillOpacity.TabIndex = 13;
            this.selectFillOpacity.Text = "Opacity";
            this.selectFillOpacity.UseVisualStyleBackColor = true;
            this.selectFillOpacity.Click += new System.EventHandler(this.selectFillOpacityClick);
            // 
            // selectDeleteItem
            // 
            this.selectDeleteItem.Location = new System.Drawing.Point(495, 3);
            this.selectDeleteItem.Name = "selectDeleteItem";
            this.selectDeleteItem.Size = new System.Drawing.Size(75, 23);
            this.selectDeleteItem.TabIndex = 12;
            this.selectDeleteItem.Text = "Delete Item";
            this.selectDeleteItem.UseVisualStyleBackColor = true;
            // 
            // selectStrokeValue
            // 
            this.selectStrokeValue.Location = new System.Drawing.Point(208, 4);
            this.selectStrokeValue.Name = "selectStrokeValue";
            this.selectStrokeValue.Size = new System.Drawing.Size(20, 20);
            this.selectStrokeValue.TabIndex = 11;
            this.selectStrokeValue.UseVisualStyleBackColor = true;
            // 
            // selectFillValue
            // 
            this.selectFillValue.Location = new System.Drawing.Point(36, 4);
            this.selectFillValue.Name = "selectFillValue";
            this.selectFillValue.Size = new System.Drawing.Size(20, 20);
            this.selectFillValue.TabIndex = 10;
            this.selectFillValue.UseVisualStyleBackColor = true;
            // 
            // selectStrokeWidthValue
            // 
            this.selectStrokeWidthValue.AutoSize = true;
            this.selectStrokeWidthValue.Location = new System.Drawing.Point(399, 8);
            this.selectStrokeWidthValue.Name = "selectStrokeWidthValue";
            this.selectStrokeWidthValue.Size = new System.Drawing.Size(13, 13);
            this.selectStrokeWidthValue.TabIndex = 9;
            this.selectStrokeWidthValue.Text = "1";
            // 
            // selectDepthValue
            // 
            this.selectDepthValue.AutoSize = true;
            this.selectDepthValue.Location = new System.Drawing.Point(471, 8);
            this.selectDepthValue.Name = "selectDepthValue";
            this.selectDepthValue.Size = new System.Drawing.Size(13, 13);
            this.selectDepthValue.TabIndex = 8;
            this.selectDepthValue.Text = "1";
            // 
            // SideUIPanel
            // 
            this.SideUIPanel.Controls.Add(this.clearDrawing);
            this.SideUIPanel.Controls.Add(this.saveAsButton);
            this.SideUIPanel.Controls.Add(this.openSVGButton);
            this.SideUIPanel.Controls.Add(this.panel2);
            this.SideUIPanel.Controls.Add(this.exportAsBitmapButton);
            this.SideUIPanel.Controls.Add(this.saveButton);
            this.SideUIPanel.Location = new System.Drawing.Point(508, 0);
            this.SideUIPanel.Name = "SideUIPanel";
            this.SideUIPanel.Size = new System.Drawing.Size(112, 223);
            this.SideUIPanel.TabIndex = 11;
            // 
            // clearDrawing
            // 
            this.clearDrawing.Location = new System.Drawing.Point(7, 123);
            this.clearDrawing.Name = "clearDrawing";
            this.clearDrawing.Size = new System.Drawing.Size(92, 23);
            this.clearDrawing.TabIndex = 19;
            this.clearDrawing.Text = "Clear";
            this.clearDrawing.UseVisualStyleBackColor = true;
            this.clearDrawing.Click += new System.EventHandler(this.clearDrawingClick);
            // 
            // saveAsButton
            // 
            this.saveAsButton.Location = new System.Drawing.Point(7, 36);
            this.saveAsButton.Name = "saveAsButton";
            this.saveAsButton.Size = new System.Drawing.Size(92, 23);
            this.saveAsButton.TabIndex = 18;
            this.saveAsButton.Text = "Save As";
            this.saveAsButton.UseVisualStyleBackColor = true;
            this.saveAsButton.Click += new System.EventHandler(this.saveAsSVG);
            // 
            // openSVGButton
            // 
            this.openSVGButton.Location = new System.Drawing.Point(7, 65);
            this.openSVGButton.Name = "openSVGButton";
            this.openSVGButton.Size = new System.Drawing.Size(92, 23);
            this.openSVGButton.TabIndex = 17;
            this.openSVGButton.Text = "Open";
            this.openSVGButton.UseVisualStyleBackColor = true;
            this.openSVGButton.Click += new System.EventHandler(this.openSVGClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Location = new System.Drawing.Point(0, 229);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(112, 223);
            this.panel2.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 36);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Export Bitmap";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(7, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Export SVG";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // exportAsBitmapButton
            // 
            this.exportAsBitmapButton.Location = new System.Drawing.Point(7, 94);
            this.exportAsBitmapButton.Name = "exportAsBitmapButton";
            this.exportAsBitmapButton.Size = new System.Drawing.Size(92, 23);
            this.exportAsBitmapButton.TabIndex = 1;
            this.exportAsBitmapButton.Text = "Export Bitmap";
            this.exportAsBitmapButton.UseVisualStyleBackColor = true;
            this.exportAsBitmapButton.Click += new System.EventHandler(this.exportPNGClick);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(7, 7);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(92, 23);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveSVG);
            // 
            // SideUIPanelTwo
            // 
            this.SideUIPanelTwo.Controls.Add(this.itemAngle);
            this.SideUIPanelTwo.Controls.Add(this.label7);
            this.SideUIPanelTwo.Controls.Add(this.FontUIPanel);
            this.SideUIPanelTwo.Controls.Add(this.itemHeight);
            this.SideUIPanelTwo.Controls.Add(this.label4);
            this.SideUIPanelTwo.Controls.Add(this.itemWidth);
            this.SideUIPanelTwo.Controls.Add(this.label3);
            this.SideUIPanelTwo.Controls.Add(this.itemY);
            this.SideUIPanelTwo.Controls.Add(this.label2);
            this.SideUIPanelTwo.Controls.Add(this.itemX);
            this.SideUIPanelTwo.Controls.Add(this.label1);
            this.SideUIPanelTwo.Location = new System.Drawing.Point(508, 229);
            this.SideUIPanelTwo.Name = "SideUIPanelTwo";
            this.SideUIPanelTwo.Size = new System.Drawing.Size(112, 360);
            this.SideUIPanelTwo.TabIndex = 12;
            // 
            // itemAngle
            // 
            this.itemAngle.Location = new System.Drawing.Point(3, 328);
            this.itemAngle.Name = "itemAngle";
            this.itemAngle.Size = new System.Drawing.Size(92, 20);
            this.itemAngle.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 312);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Item Angle:";
            // 
            // FontUIPanel
            // 
            this.FontUIPanel.Controls.Add(this.itemText);
            this.FontUIPanel.Controls.Add(this.label5);
            this.FontUIPanel.Controls.Add(this.label6);
            this.FontUIPanel.Controls.Add(this.itemFontName);
            this.FontUIPanel.Controls.Add(this.itemFontButton);
            this.FontUIPanel.Controls.Add(this.itemFontSize);
            this.FontUIPanel.Location = new System.Drawing.Point(0, 22);
            this.FontUIPanel.Name = "FontUIPanel";
            this.FontUIPanel.Size = new System.Drawing.Size(112, 127);
            this.FontUIPanel.TabIndex = 12;
            this.FontUIPanel.Visible = false;
            // 
            // itemText
            // 
            this.itemText.Location = new System.Drawing.Point(4, 34);
            this.itemText.Name = "itemText";
            this.itemText.Size = new System.Drawing.Size(92, 20);
            this.itemText.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Text:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Font:";
            // 
            // itemFontName
            // 
            this.itemFontName.AutoSize = true;
            this.itemFontName.Location = new System.Drawing.Point(4, 70);
            this.itemFontName.Name = "itemFontName";
            this.itemFontName.Size = new System.Drawing.Size(23, 13);
            this.itemFontName.TabIndex = 10;
            this.itemFontName.Text = "null";
            // 
            // itemFontButton
            // 
            this.itemFontButton.Location = new System.Drawing.Point(7, 99);
            this.itemFontButton.Name = "itemFontButton";
            this.itemFontButton.Size = new System.Drawing.Size(92, 23);
            this.itemFontButton.TabIndex = 9;
            this.itemFontButton.Text = "Change Font";
            this.itemFontButton.UseVisualStyleBackColor = true;
            // 
            // itemFontSize
            // 
            this.itemFontSize.AutoSize = true;
            this.itemFontSize.Location = new System.Drawing.Point(4, 83);
            this.itemFontSize.Name = "itemFontSize";
            this.itemFontSize.Size = new System.Drawing.Size(23, 13);
            this.itemFontSize.TabIndex = 8;
            this.itemFontSize.Text = "null";
            // 
            // itemHeight
            // 
            this.itemHeight.Location = new System.Drawing.Point(4, 289);
            this.itemHeight.Name = "itemHeight";
            this.itemHeight.Size = new System.Drawing.Size(92, 20);
            this.itemHeight.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 272);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Item Height:";
            // 
            // itemWidth
            // 
            this.itemWidth.Location = new System.Drawing.Point(4, 249);
            this.itemWidth.Name = "itemWidth";
            this.itemWidth.Size = new System.Drawing.Size(92, 20);
            this.itemWidth.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Item Width:";
            // 
            // itemY
            // 
            this.itemY.Location = new System.Drawing.Point(4, 209);
            this.itemY.Name = "itemY";
            this.itemY.Size = new System.Drawing.Size(92, 20);
            this.itemY.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Item Y:";
            // 
            // itemX
            // 
            this.itemX.Location = new System.Drawing.Point(4, 169);
            this.itemX.Name = "itemX";
            this.itemX.Size = new System.Drawing.Size(92, 20);
            this.itemX.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item X:";
            // 
            // invisibleButton
            // 
            this.invisibleButton.Location = new System.Drawing.Point(578, 609);
            this.invisibleButton.Name = "invisibleButton";
            this.invisibleButton.Size = new System.Drawing.Size(41, 23);
            this.invisibleButton.TabIndex = 13;
            this.invisibleButton.UseVisualStyleBackColor = true;
            this.invisibleButton.Visible = false;
            // 
            // previewPanel
            // 
            this.previewPanel.Location = new System.Drawing.Point(84, 6);
            this.previewPanel.Name = "previewPanel";
            this.previewPanel.Size = new System.Drawing.Size(400, 568);
            this.previewPanel.TabIndex = 11;
            this.previewPanel.Visible = false;
            // 
            // SchetsPlus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 683);
            this.Controls.Add(this.invisibleButton);
            this.Controls.Add(this.SideUIPanelTwo);
            this.Controls.Add(this.SideUIPanel);
            this.Controls.Add(this.BottomUIPanel);
            this.Controls.Add(this.MainDrawingPanel);
            this.Controls.Add(this.TopUIPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SchetsPlus";
            this.Text = "SchetsPlus";
            this.Resize += new System.EventHandler(this.formResize);
            this.TopUIPanel.ResumeLayout(false);
            this.MainDrawingPanel.ResumeLayout(false);
            this.BottomUIPanel.ResumeLayout(false);
            this.BottomUIPanel.PerformLayout();
            this.SideUIPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.SideUIPanelTwo.ResumeLayout(false);
            this.SideUIPanelTwo.PerformLayout();
            this.FontUIPanel.ResumeLayout(false);
            this.FontUIPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button toolNone;
        private System.Windows.Forms.Button toolPen;
        private System.Windows.Forms.Button toolCurve;
        private System.Windows.Forms.Button toolRect;
        private System.Windows.Forms.Button selectFill;
        private System.Windows.Forms.Button selectStroke;
        private System.Windows.Forms.Button selectStrokeWidth;
        private System.Windows.Forms.Button selectDepth;
        private System.Windows.Forms.Panel TopUIPanel;
        private System.Windows.Forms.Panel MainDrawingPanel;
        private System.Windows.Forms.Panel BottomUIPanel;
        private System.Windows.Forms.Panel SideUIPanel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button selectStrokeValue;
        private System.Windows.Forms.Button selectFillValue;
        private System.Windows.Forms.Label selectStrokeWidthValue;
        private System.Windows.Forms.Label selectDepthValue;
        private System.Windows.Forms.Button toolEllipse;
        private System.Windows.Forms.Button selectDeleteItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel SideUIPanelTwo;
        private System.Windows.Forms.TextBox itemX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox itemHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox itemWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox itemY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button openSVGButton;
        private TransparentPanel previewPanel;
        private System.Windows.Forms.Button invisibleButton;
        private System.Windows.Forms.Label selectStrokeOpacityValue;
        private System.Windows.Forms.Button selectStrokeOpacity;
        private System.Windows.Forms.Label selectFillOpacityValue;
        private System.Windows.Forms.Button selectFillOpacity;
        private System.Windows.Forms.Button exportAsBitmapButton;
        private System.Windows.Forms.Button saveAsButton;
        private System.Windows.Forms.Button toolText;
        private System.Windows.Forms.Panel FontUIPanel;
        private System.Windows.Forms.Label itemFontSize;
        private System.Windows.Forms.Button itemFontButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label itemFontName;
        private System.Windows.Forms.TextBox itemText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button clearDrawing;
        private System.Windows.Forms.TextBox itemAngle;
        private System.Windows.Forms.Label label7;
    }
}

