﻿using SchetsPlusLibrary;
using SchetsPlusSVGParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

//Toelichting
//Ik (Douwe) gebruik al lange tijd Inkscape, daar heb ik mijn ideeën uit gehaald. 
//Ik ben altijd veeleisend over mijn code en ik vind het gewoon erg leuk om te 
//doen dus ik zag al aankomen dat ik het hele voorbeeld helmaal zou herschrijven
//daarom zijn we maar vanaf scratch begonnen. Om alle vrijheid te hebben het 
//programma zo goed mogelijk te schrijven. En als je de voorbeeldcode mee zou
//nemen zou toch letterlijk alles herschreven moeten worden, op de menustructuur
//na maar die wilde ik sowieso overhoop halen. En uit de opdracht blijkt dat het 
//niet verplicht is de voorbeeldcode te gebruiken (2e zin: Je hoeft...).

//Onze git link:https://bitbucket.org/teamawesome123456/schetsplus (alleen voor de zekerheid)
//Het goede startupobject is: SchetsPlusUI.csproj
//Er zijn drie projecten omdat dan de code beter uit elkaar gehaald kan worden 
//en eventueel kan worden hergebruikt. En dat laatste is een reele mogelijkheid,
//in dit programma zitten een paar stukjes van eerder door ons geschreven programma's.

//We hebben een usermanual gemaakt, het bestand staat in de toplevel folder. (schetsplus-manual.pdf)

//Assignment:
//   Text           Done
// 1 Circles        Done
// 2 Exporting      Done      
// 3 Deleting       Done
// 4 Saving         Done
//Extra:
// 5 1 Undo         Done
// 5 2 StrokeWidth  Done
// 5 3 Many colours Done
// 5 4 Change depth Done
// 5 5 Moving       Done
//
// Things I want: (Douwe)
// D Grouping objects
// D Degrouping objects
// D Document preview (having a a4 by example)
// D Zooming/panning (seeing more or less details)
// D Copying shapes
// D Copying groups
// D Moving objects with arrow keys
// D Hotkeys
// D Guide (so people know all our little neet tricks)
// D Asking the user if he/she/it wants to keep the changes
// D Context menu
// D Text fonts UI
// D Rotating shapes
// D Bezier curves
//1/2Easy way to set not color in fill or stroke (alpha = 0) (easy way to set alpha)
// * Dashed lines UI (the code is done)
// * Sorting options (to place all selected on the same x/y or at the middle etc)
// * Better depth UI (btns for up and down)
// * Icons on the UI (make them with this program ;-) )
// * Better Ico exporting (options for making square and choosing sizes)
//
// Things that need to be done before handing it in:
// D Moving and resizing should also preview.
// D The previewpanel should invalidate properly
// D Importing/exporting Text
// D Importing/Exporting freelines
// D PreviewPanel no background when previewing.
// D Add curve

namespace SchetsPlusUI
{
    public partial class SchetsPlus : Form
    {
        enum SelectedTool { None, Pen, Curve, Rectangle, Ellipse, Text };
        List<Shape> shapes = new List<Shape>();
        Color selectedFillColor = Color.White;
        int selectedFillOpacity = 255;
        Color selectedStrokeColor = Color.Black;
        float selectedStrokeWidth = 1;
        int selectedStrokeOpacity = 255;
        int selectedDepth = 0;
        SelectedTool selectedTool = SelectedTool.None;
        List<FPoint> pointList = new List<FPoint>();
        List<FPoint> curvePointList = new List<FPoint>();
        Shape preview;
        Shape selectedItem;
        Shape document = new Rect(0, 0, 1900, 2100);
        Shape clipboard;
        bool movingObject = false;
        bool reshapingObject = false;
        bool resizingObject = false;
        bool rotatingObject = false;
        bool selectingObjects = false;
        bool panning = false;
        bool mouseDown = false;
        bool unsavedChanges = false;
        bool isDrawingCurve = false;
        FPoint viewBoxOffset = new FPoint(0, 0);
        float zoomFactor = 1;
        int draggingPoint = -1;
        string formTitle = "SchetsPlus";
        Image bufferImage;
        ContextMenuStrip context;
        string filename = "";
        List<List<Shape>> history = new List<List<Shape>>();
        int historyLevel = -1;

        public SchetsPlus()
        {
            defaultConstructor();
        }

        private void defaultConstructor()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            InitializeComponent();

            updateViewBoxSettings();
            this.DoubleBuffered = true;
            formResize(null, null);
            updateSelectButtons();
            updateToolButtons();
            previewPanel.Paint += previewPanelPaint;
            previewPanel.MouseDown += mainPanelMouseDown;
            previewPanel.MouseMove += mainPanelMouseMove;
            previewPanel.MouseUp += mainPanelMouseUp;

            previewPanel.BackColor = Color.Gray;

            foreach (Control c in TopUIPanel.Controls)
            {
                c.Click += toolButtonClick;
            }

            itemX.KeyDown += itemTextBoxChange;
            itemX.LostFocus += itemTextBoxChange;
            itemY.KeyDown += itemTextBoxChange;
            itemY.LostFocus += itemTextBoxChange;
            itemWidth.KeyDown += itemTextBoxChange;
            itemWidth.LostFocus += itemTextBoxChange;
            itemHeight.KeyDown += itemTextBoxChange;
            itemHeight.LostFocus += itemTextBoxChange;
            itemAngle.KeyDown += itemTextBoxChange;
            itemAngle.LostFocus += itemTextBoxChange;
            itemText.KeyDown += itemTextBoxChange;
            itemText.LostFocus += itemTextBoxChange;
            itemFontButton.Click += (o, i) => { var fd = new FontDialog() { Font = ((Text)selectedItem).Font }; if (fd.ShowDialog() == DialogResult.OK) { ((Text)selectedItem).Font = fd.Font; updateMainPanel(); updateItemButtons(); addToHistory(); }; };

            MainDrawingPanel.MouseWheel += zoom;
            selectFill.Click += selectFillClick;
            selectStroke.Click += selectStrokeClick;
            selectStrokeWidth.Click += selectStrokeWidthClick;
            selectDepth.Click += selectDepthClick;
            selectDeleteItem.Click += selectDeleteItemClick;
            this.KeyPreview = true;
            this.KeyUp += hotkeysUp;
            this.KeyDown += hotkeysDown;
            this.FormClosing += close;

            updateMainPanel();

            context = new ContextMenuStrip();
            context.Opening += contextOpen;
            this.ContextMenuStrip = context;
            addToHistory();
        }

        public SchetsPlus(string filename)
        {
            defaultConstructor();
            if (filename != "") openSVG(filename);
        }

        private void itemTextBoxChange(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) itemTextBoxChange(sender, (EventArgs)e);
        }

        private void itemTextBoxChange(object sender, EventArgs e)
        {
            try
            {
                selectedItem.X = (float)Convert.ToDouble(itemX.Text);
                selectedItem.Y = (float)Convert.ToDouble(itemY.Text);
                selectedItem.Width = (float)Convert.ToDouble(itemWidth.Text);
                selectedItem.Height = (float)Convert.ToDouble(itemHeight.Text);
                selectedItem.Angle = (float)Convert.ToDouble(itemAngle.Text);
                if (selectedItem.GetType() == typeof(Text)) ((Text)selectedItem).Content = itemText.Text; updateMainPanel();
                updateItemButtons();
                updateMainPanel();
                addToHistory();
            }
            catch { }
        }

        private void toolButtonClick(object sender, EventArgs e)
        {
            if (sender == toolNone) selectedTool = SelectedTool.None;
            else deselectAll();

            if (sender == toolPen) selectedTool = SelectedTool.Pen;
            else if (sender == toolCurve) selectedTool = SelectedTool.Curve;
            else if (sender == toolRect) selectedTool = SelectedTool.Rectangle;
            else if (sender == toolEllipse) selectedTool = SelectedTool.Ellipse;
            else if (sender == toolText) selectedTool = SelectedTool.Text;

            invisibleButton.Focus();
            invisibleButton.Select();

            updateToolButtons();
        }

        private void hotkeysUp(object sender, KeyEventArgs e)
        {
            Keys k = e.KeyCode;
            bool shift = ModifierKeys == Keys.Shift;
            bool ctrl = ModifierKeys == Keys.Control;
            if (itemText.Focused != true)
            {
                if (k == Keys.O && ctrl) openSVGClick(null, null);
                else if (k == Keys.S && ctrl) saveSVG(null, null);
                else if (k == Keys.E && ctrl) exportPNGClick(null, null);
                else if (k == Keys.C && ctrl) copySelection(null, null);
                else if (k == Keys.V && ctrl) pasteClipboard(null, null);
                else if (k == Keys.Z && ctrl) turnBackHistory();
                else if (k == Keys.Y && ctrl) turnBackHistoryBack();
                else if (k == Keys.F && shift) selectFillClick(null, null);
                else if (k == Keys.S && shift) selectStrokeClick(null, null);
                else if (k == Keys.W && shift) selectStrokeWidthClick(null, null);
                else if (k == Keys.D && shift) selectDepthClick(null, null);
                else if (k == Keys.G && ctrl) groupSelection(null, null);
                else if (k == Keys.G && shift) deGroupSelection(null, null);
                else if (k == Keys.O && shift) selectFillOpacityClick(null, null);
                else if (k == Keys.P && shift) selectStrokeOpacityClick(null, null);
                else if (k == Keys.Q) qPressed();
                else if (k == Keys.Escape) escapePressed();
                else if (k == Keys.S) toolButtonClick(toolNone, null);
                else if (k == Keys.P) toolButtonClick(toolPen, null);
                else if (k == Keys.B) toolButtonClick(toolCurve, null);
                else if (k == Keys.R) toolButtonClick(toolRect, null);
                else if (k == Keys.E) toolButtonClick(toolEllipse, null);
                else if (k == Keys.T) toolButtonClick(toolText, null);
                else if (k == Keys.Delete) selectDeleteItemClick(null, null);

            }
        }

        private void hotkeysDown(object sender, KeyEventArgs e)
        {
            if (itemText.Focused != true)
            {
                Keys k = e.KeyCode;
                float delta = 10 * zoomFactor;

                if (ModifierKeys == Keys.Shift)
                {
                    delta *= 10;
                }
                else if (ModifierKeys == Keys.Alt)
                {
                    delta /= 10;
                }
                if (selectedItem != null)
                {
                    if (k == Keys.Up) { selectedItem.Y -= delta; updateMainPanel(); }
                    if (k == Keys.Down) { selectedItem.Y += delta; updateMainPanel(); }
                    if (k == Keys.Right) { selectedItem.X += delta; updateMainPanel(); }
                    if (k == Keys.Left) { selectedItem.X -= delta; updateMainPanel(); }
                }
                else
                {
                    if (k == Keys.Up) { viewBoxOffset.Y -= delta; updateMainPanel(); }
                    if (k == Keys.Down) { viewBoxOffset.Y += delta; updateMainPanel(); }
                    if (k == Keys.Right) { viewBoxOffset.X += delta; updateMainPanel(); }
                    if (k == Keys.Left) { viewBoxOffset.X -= delta; updateMainPanel(); }
                }
            }
        }

        private void copySelection(object sender, EventArgs e) { clipboard = selectedItem; }

        private void pasteClipboard(object sender, EventArgs e)
        {
            if (clipboard != null)
            {
                shapes.Add(clipboard.Clone(10 * zoomFactor, 10 * zoomFactor));
                updateMainPanel();
                addToHistory();
            }
        }

        private void selectDeleteItemClick(object sender, EventArgs e)
        {
            for (int i = 0; i < shapes.Count; i++)
            {
                if (shapes[i].Selected)
                {
                    shapes.Remove(shapes[i]);
                    i--;
                    unsavedChanges = true;
                }
            }
            sortShapes();
            updateMainPanel();
            addToHistory();
        }

        private void selectDepthClick(object sender, EventArgs e)
        {
            int id = (int)FloatDialog.ShowDialog("Depth", selectedDepth.ToString());
            if (id != -1)
            {
                if (id < 0)
                {
                    MessageBox.Show("The value you entered for the depth is not valid (it should not be less then zero).");
                }
                else
                {
                    foreach (Shape s in shapes)
                    {
                        if (s.Selected)
                        {
                            s.Depth = id;
                            unsavedChanges = true;
                        }
                    }
                }
            }
            sortShapes();
            updateMainPanel();
            addToHistory();
        }

        private void selectStrokeWidthClick(object sender, EventArgs e)
        {
            float id = FloatDialog.ShowDialog("StrokeWidth", selectedStrokeWidth.ToString());
            if (id != -1)
            {
                if (id < 0)
                {
                    MessageBox.Show("The value you entered for the stroke width is not valid (it should not be less then zero).");
                }
                else
                {
                    selectedStrokeWidth = id;
                    foreach (Shape s in shapes)
                    {
                        if (s.Selected)
                        {
                            s.StrokeWidth = id;
                            unsavedChanges = true;
                        }
                    }
                }
            }
            updateSelectButtons();
            updateMainPanel();
            addToHistory();
        }

        private void selectStrokeClick(object sender, EventArgs e)
        {
            var cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
            {
                selectedStrokeColor = cd.Color;
                foreach (Shape s in shapes)
                {
                    if (s.Selected)
                    {
                        s.Stroke = cd.Color;
                        unsavedChanges = true;
                    }
                }
            }
            updateSelectButtons();
            updateMainPanel();
            addToHistory();
        }

        private void selectStrokeOpacityClick(object sender, EventArgs e)
        {
            int id = (int)FloatDialog.ShowDialog("Stroke Opacity", selectedStrokeOpacity.ToString());
            if (id != -1)
            {
                if (id > 255 || id < 0)
                {
                    MessageBox.Show("The value you entered for the stroke opacity is not valid (it should be between 0 and 255).");
                }
                else
                {
                    selectedStrokeOpacity = id;
                    foreach (Shape s in shapes)
                    {
                        if (s.Selected)
                        {
                            s.StrokeOpacity = id;
                            unsavedChanges = true;
                        }
                    }
                }
            }
            updateSelectButtons();
            updateMainPanel();
            addToHistory();
        }

        private void selectFillClick(object sender, EventArgs e)
        {
            var cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
            {
                selectedFillColor = cd.Color;
                foreach (Shape s in shapes)
                {
                    if (s.Selected)
                    {
                        s.Fill = cd.Color;
                        unsavedChanges = true;
                    }
                }
            }
            updateSelectButtons();
            updateMainPanel();
            addToHistory();
        }

        private void selectFillOpacityClick(object sender, EventArgs e)
        {
            int id = (int)FloatDialog.ShowDialog("Fill Opacity", selectedFillOpacity.ToString());
            if (id != -1)
            {
                if (id > 255 || id < 0)
                {
                    MessageBox.Show("The value you entered for the fill opacity is not valid (it should be between 0 and 255).");
                }
                else
                {
                    selectedFillOpacity = id;
                    foreach (Shape s in shapes)
                    {
                        if (s.Selected)
                        {
                            s.FillOpacity = id;
                            unsavedChanges = true;
                        }
                    }
                }
            }
            updateSelectButtons();
            updateMainPanel();
            addToHistory();
        }

        private void updateMainPanel() { MainDrawingPanel.Invalidate(); }

        private void mainPanelPaint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.SmoothingMode = SmoothingMode.HighQuality;

            if (document != null) document.Draw(gr);

            foreach (Shape s in shapes)
            {
                if (previewPanel.Enabled == true)
                {
                    if (!s.Selected) s.Draw(gr);
                }
                else s.Draw(gr);
            }
        }

        private void previewPanelPaint(object sender, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;
            gr.SmoothingMode = SmoothingMode.HighQuality;
            if (bufferImage != null)
            {
                gr.FillRectangle(new SolidBrush(Color.FromArgb(228, 228, 228)), gr.VisibleClipBounds);
                gr.DrawImage(bufferImage, Point.Empty);
            }
            if (selectedItem != null) selectedItem.Draw(gr);
            if (preview != null) preview.Draw(gr);
        }

        float mouseStartX = -1;
        float mouseStartY = -1;
        float mouseStartTwoX = -1;
        float mouseStartTwoY = -1;
        float mouseStartTX = -1;
        float mouseStartTY = -1;
        float mousePreviousX = -1;
        float mousePreviousY = -1;
        bool lockX = false;
        bool lockY = false;
        FPoint originalvbo;
        int zoomDirectionNumber;
        float startingAngle = -1;

        private void mainPanelMouseDown(object sender, MouseEventArgs e)
        {
            float x = e.X;
            float y = e.Y;
            float tx = ((float)e.X * zoomFactor) + viewBoxOffset.X;
            float ty = ((float)e.Y * zoomFactor) + viewBoxOffset.Y;

            if (e.Button == MouseButtons.Left)
            {
                mouseDown = true;

                if (mouseStartX == -1)
                {
                    mouseStartX = tx;
                    mouseStartY = ty;
                }

                if (selectedTool == SelectedTool.None && selectedItem == null)
                {
                    //Group selecting things
                    selectingObjects = true;
                    updatePreviewPanel(true);
                    preview = new Rect((tx - viewBoxOffset.X) / zoomFactor, (ty - viewBoxOffset.Y) / zoomFactor, 0, 0, false)
                    {
                        Fill = Color.FromArgb(0, 0, 0, 0),
                        FillOpacity = 0,
                        StrokeDashes = new float[] { 5, 5 },
                        ViewBoxOffset = viewBoxOffset,
                        ZoomFactor = zoomFactor
                    };
                }

                if (selectedTool == SelectedTool.None && selectedItem != null && ModifierKeys == Keys.Alt)
                {
                    //Rotating things
                    updatePreviewPanel(true);
                    float[] rr = selectedItem.ResizeRectangles;
                    float w = selectedItem.ResizeRectangleSize;
                    for (int i = 0; i < rr.Length; i += 2)
                    {
                        if (tx > rr[i] && tx < rr[i] + w && ty > rr[i + 1] && ty < rr[i + 1] + w)
                        {
                            rotatingObject = true;
                            Cursor.Current = Cursors.Cross;
                            zoomDirectionNumber = i;
                            float dx = (tx - selectedItem.RotatePoint.X);
                            float dy = (ty - selectedItem.RotatePoint.Y);
                            startingAngle = (float)(Math.Atan2((double)dy, (double)dx) / (Math.PI) * 180);
                        }
                    }
                }

                if (selectedTool == SelectedTool.None && selectedItem != null && !rotatingObject)
                {
                    //Resizing things
                    updatePreviewPanel(true);
                    
                    if (selectedItem is Curve)
                    {
                        

                        Curve curve = selectedItem as Curve;
                        float[] sr = curve.ShapingRectangles;
                        float w = curve.ResizeRectangleSize;
                        for (int i = 0; i < sr.Length; i += 2)
                        {
                            if (tx > sr[i] && tx < sr[i] + w && ty > sr[i + 1] && ty < sr[i + 1] + w)
                            {
                                Cursor.Current = Cursors.SizeAll;
                                draggingPoint = i/2;
                                reshapingObject = true;
                            }
                        }
                    }
                    else
                    {
                        reshapingObject = false;
                        float[] rr = selectedItem.ResizeRectangles;
                        float w = selectedItem.ResizeRectangleSize;
                        for (int i = 0; i < rr.Length; i += 2)
                        {
                            if (tx > rr[i] && tx < rr[i] + w && ty > rr[i + 1] && ty < rr[i + 1] + w)
                            {
                                resizingObject = true;

                                /* For the codes:
                                 * 0  -  2 -  4
                                 * |          |
                                 * 6          8
                                 * |          |
                                 * 10 - 12 - 14 */

                                lockX = false;
                                lockY = false;
                                mouseStartTX = tx;
                                mouseStartTY = ty;

                                zoomDirectionNumber = i;

                                switch (i)
                                {
                                    case 0:
                                        Cursor.Current = Cursors.SizeNWSE;
                                        mouseStartX = selectedItem.X + selectedItem.Width;
                                        mouseStartY = selectedItem.Y + selectedItem.Height;
                                        mouseStartTwoX = selectedItem.X;
                                        mouseStartTwoY = selectedItem.Y;
                                        break;
                                    case 2:
                                        Cursor.Current = Cursors.SizeNS;
                                        lockX = true;
                                        mouseStartX = selectedItem.X;
                                        mouseStartY = selectedItem.Y + selectedItem.Height;
                                        mouseStartTwoX = selectedItem.X + selectedItem.Width;
                                        mouseStartTwoY = selectedItem.Y;
                                        break;
                                    case 4:
                                        Cursor.Current = Cursors.SizeNESW;
                                        mouseStartX = selectedItem.X;
                                        mouseStartY = selectedItem.Y + selectedItem.Height;
                                        mouseStartTwoX = selectedItem.X + selectedItem.Width;
                                        mouseStartTwoY = selectedItem.Y;
                                        break;
                                    case 6:
                                        Cursor.Current = Cursors.SizeWE;
                                        lockY = true;
                                        mouseStartX = selectedItem.X + selectedItem.Width;
                                        mouseStartY = selectedItem.Y;
                                        mouseStartTwoX = selectedItem.X;
                                        mouseStartTwoY = selectedItem.Y + selectedItem.Height;
                                        break;
                                    case 8:
                                        Cursor.Current = Cursors.SizeWE;
                                        lockY = true;
                                        mouseStartX = selectedItem.X;
                                        mouseStartY = selectedItem.Y;
                                        mouseStartTwoX = selectedItem.X + selectedItem.Width;
                                        mouseStartTwoY = selectedItem.Y + selectedItem.Height;
                                        break;
                                    case 10:
                                        Cursor.Current = Cursors.SizeNESW;
                                        mouseStartX = selectedItem.X + selectedItem.Width;
                                        mouseStartY = selectedItem.Y;
                                        mouseStartTwoX = selectedItem.X;
                                        mouseStartTwoY = selectedItem.Y + selectedItem.Height;
                                        break;
                                    case 12:
                                        Cursor.Current = Cursors.SizeNS;
                                        lockX = true;
                                        mouseStartX = selectedItem.X;
                                        mouseStartY = selectedItem.Y;
                                        mouseStartTwoX = selectedItem.X + selectedItem.Width;
                                        mouseStartTwoY = selectedItem.Y + selectedItem.Height;
                                        break;
                                    case 14:
                                        Cursor.Current = Cursors.SizeNWSE;
                                        mouseStartX = selectedItem.X;
                                        mouseStartY = selectedItem.Y;
                                        mouseStartTwoX = selectedItem.X + selectedItem.Width;
                                        mouseStartTwoY = selectedItem.Y + selectedItem.Height;
                                        break;
                                }
                                break;
                            }
                        }
                    }
                }
                    
                    
                if (selectedTool == SelectedTool.None && selectedItem != null && !resizingObject && !reshapingObject && insideShape(tx, ty, selectedItem))
                {
                    //Moving things
                    updatePreviewPanel(true);
                    movingObject = true;
                    Cursor.Current = Cursors.Hand;
                    mouseStartX = tx - selectedItem.X;
                    mouseStartY = ty - selectedItem.Y;
                }

                if (selectedTool == SelectedTool.Rectangle && preview == null)
                {
                    //Rectangle preview
                    updatePreviewPanel(true);
                    preview = new Rect(tx, ty, 0, 0)
                    {
                        Fill = selectedFillColor,
                        Stroke = selectedStrokeColor,
                        StrokeWidth = selectedStrokeWidth,
                        StrokeOpacity = selectedStrokeOpacity,
                        FillOpacity = selectedStrokeOpacity,
                        ViewBoxOffset = viewBoxOffset,
                        ZoomFactor = zoomFactor
                    };
                }

                if (selectedTool == SelectedTool.Ellipse && preview == null)
                {
                    //Ellipse preview
                    updatePreviewPanel(true);
                    preview = new Ellipse(tx, ty, 0, 0)
                    {
                        Fill = selectedFillColor,
                        Stroke = selectedStrokeColor,
                        StrokeWidth = selectedStrokeWidth,
                        StrokeOpacity = selectedStrokeOpacity,
                        FillOpacity = selectedStrokeOpacity,
                        ViewBoxOffset = viewBoxOffset,
                        ZoomFactor = zoomFactor
                    };
                }
                if (selectedTool == SelectedTool.Pen && preview == null)
                {
                    //Drawing with the pen
                    updatePreviewPanel(true);
                    preview = new FreeLine(tx, ty, selectedStrokeWidth, pointList)
                    {
                        Stroke = selectedStrokeColor,
                        StrokeWidth = selectedStrokeWidth,
                        StrokeOpacity = selectedStrokeOpacity,
                        ViewBoxOffset = viewBoxOffset,
                        ZoomFactor = zoomFactor
                    };
                }
                if (selectedTool == SelectedTool.Curve)
                {
                    //Drawing with the Bezier tool
                    if (isDrawingCurve)
                    {
                        curvePointList.Add(new FPoint(tx, ty));
                        pointList = new List<FPoint>(curvePointList.Select(c => c.Copy()));
                        preview = new Curve(tx, ty, selectedStrokeWidth, pointList)
                        {
                            Stroke = selectedStrokeColor,
                            StrokeWidth = selectedStrokeWidth,
                            StrokeOpacity = selectedStrokeOpacity,
                            ViewBoxOffset = viewBoxOffset,
                            ZoomFactor = zoomFactor
                        };
                    }
                    else
                    {
                        updatePreviewPanel(true);
                        curvePointList.Add(new FPoint(tx, ty));
                        pointList = new List<FPoint>(curvePointList.Select(c => c.Copy()));
                        preview = new Curve(tx, ty, selectedStrokeWidth, pointList)
                        {
                            Stroke = selectedStrokeColor,
                            StrokeWidth = selectedStrokeWidth,
                            StrokeOpacity = selectedStrokeOpacity,
                            ViewBoxOffset = viewBoxOffset,
                            ZoomFactor = zoomFactor
                        };

                        isDrawingCurve = true;
                    }
                }
            }
            else if (e.Button == MouseButtons.Middle)
            {
                //Panning
                if (panning == false)
                {
                    if (mouseStartX == -1)
                    {
                        mouseStartX = x;
                        mouseStartY = y;
                    }
                    panning = true;
                    originalvbo = viewBoxOffset;
                }
            }

        }
        private void mainPanelMouseMove(object sender, MouseEventArgs e)
        {

            float x = e.X;
            float y = e.Y;
            float tx = ((float)e.X * zoomFactor) + viewBoxOffset.X;
            float ty = ((float)e.Y * zoomFactor) + viewBoxOffset.Y;

            if (isDrawingCurve && selectedTool != SelectedTool.Curve)
            {
                isDrawingCurve = false;
                unsavedChanges = true;
                Curve curve = new Curve(mouseStartX, mouseStartY, selectedStrokeWidth, new List<FPoint>(curvePointList))
                {
                    Stroke = selectedStrokeColor,
                    StrokeWidth = selectedStrokeWidth,
                    StrokeOpacity = selectedStrokeOpacity,
                    ViewBoxOffset = viewBoxOffset,
                    ZoomFactor = zoomFactor
                };
                shapes.Add(curve);

                pointList.Clear();
                curvePointList.Clear();
                mouseStartX = -1;
                mouseStartY = -1;
                addToHistory();
                preview = null;
                mouseDown = false;
                resizingObject = false;
                movingObject = false;
                rotatingObject = false;
                mousePreviousX = -1;
                mousePreviousY = -1;
                mouseStartX = -1;
                mouseStartY = -1;
                mouseStartTwoX = -1;
                mouseStartTwoY = -1;

                updateItemButtons();
                updateSelectButtons();
                sortShapes();
                updatePreviewPanel(false);
                updateViewBoxSettings();
                updateMainPanel();
            }

            if (mouseDown)
            {
                if (selectedTool == SelectedTool.Pen)
                {
                    if (mousePreviousX == -1 || !(mousePreviousX == tx && mousePreviousY == ty))
                    {
                        pointList.Add(new FPoint(tx, ty));
                        mousePreviousX = tx;
                        mousePreviousY = ty;
                        unsavedChanges = true;
                    }
                }
            }

            if (selectedTool == SelectedTool.Curve)
            {
                if (isDrawingCurve)
                {
                    pointList = new List<FPoint>(curvePointList.Select(c => c.Copy()));
                    pointList.Add(new FPoint(tx, ty));
                    preview = new Curve(tx, ty, selectedStrokeWidth, pointList)
                    {
                        Stroke = selectedStrokeColor,
                        StrokeWidth = selectedStrokeWidth,
                        StrokeOpacity = selectedStrokeOpacity,
                        ViewBoxOffset = viewBoxOffset,
                        ZoomFactor = zoomFactor
                    };
                    updatePreviewPanel(true);
                }
                
            }
            else
            {
                if (preview != null)
                {
                    float w = tx - preview.X;
                    float h = ty - preview.Y;
                    preview.Width = w;
                    preview.Height = h;
                    unsavedChanges = true;
                }
            }


            if (selectingObjects && preview != null)
            {
                preview.Width = (tx - mouseStartX) / zoomFactor;
                preview.Height = (ty - mouseStartY) / zoomFactor;
                unsavedChanges = true;
            }
            if (reshapingObject && selectedItem != null)
            {
                updatePreviewPanel(true);
                Curve curve = selectedItem as Curve;
                FPoint p = curve.PointList.ElementAt(draggingPoint);
                curve.PointList.ElementAt(draggingPoint).X = tx;
                curve.PointList.ElementAt(draggingPoint).Y = ty;
            }
            if (selectedItem != null && movingObject == true)
            {
                selectedItem.X = tx - mouseStartX;
                selectedItem.Y = ty - mouseStartY;
                unsavedChanges = true;
                updateItemButtons();
            }

            if (selectedItem != null && rotatingObject == true)
            {
                float dx = (tx - selectedItem.RotatePoint.X);
                float dy = (ty - selectedItem.RotatePoint.Y);

                float resizeBlockFactor = 0;
                switch (zoomDirectionNumber)
                {
                    case (0):
                        resizeBlockFactor = 270;
                        break;
                    case (2):
                        resizeBlockFactor = 180;
                        break;
                    case (4):
                        resizeBlockFactor = 90;
                        break;
                    case (6):
                        resizeBlockFactor = 0;
                        break;
                    case (8):
                        resizeBlockFactor = 0;
                        break;
                    case (10):
                        resizeBlockFactor = 90;
                        break;
                    case (12):
                        resizeBlockFactor = 180;
                        break;
                    case (14):
                        resizeBlockFactor = 270;
                        break;
                }

                selectedItem.Angle = (float)(Math.Atan2((double)dy, (double)dx) / (Math.PI) * 180) + startingAngle + resizeBlockFactor;

                unsavedChanges = true;
                updateItemButtons();
            }

            if (selectedItem != null && resizingObject == true)
            {
                float x1 = mouseStartX;
                float y1 = mouseStartY;
                float x2 = mouseStartTwoX;
                float y2 = mouseStartTwoY;
                float dx = (tx - mouseStartTX);
                float dy = (ty - mouseStartTY);

                float xe = 0;
                float w = 0;
                float ye = 0;
                float h = 0;

                switch (zoomDirectionNumber)
                {
                    case 0:
                        xe = x2 + dx;
                        w = x1 - x2 - dx;
                        ye = y2 + dy;
                        h = y1 - y2 - dy;
                        break;
                    case 2:
                        xe = x1;
                        ye = y2 + dy;
                        h = y1 - y2 - dy;
                        break;
                    case 4:
                        xe = x1;
                        w = x2 - x1 + dx;
                        ye = y2 + dy;
                        h = y1 - y2 - dy;
                        break;
                    case 6:
                        xe = x2 + dx;
                        w = x1 - x2 - dx;
                        ye = y1;
                        break;
                    case 8:
                        xe = x1;
                        w = x2 - x1 + dx;
                        ye = y1;
                        break;
                    case 10:
                        xe = x2 + dx;
                        w = x1 - x2 - dx;
                        ye = y1;
                        h = y2 - y1 + dy;
                        break;
                    case 12:
                        xe = x1;
                        ye = y1;
                        h = y2 - y1 + dy;
                        break;
                    case 14:
                        xe = x1;
                        w = x2 - x1 + dx;
                        ye = y1;
                        h = y2 - y1 + dy;
                        break;
                }

                selectedItem.X = xe;
                selectedItem.Y = ye;

                if (lockX) selectedItem.Height = h;
                else if (lockY) selectedItem.Width = w;
                else
                {
                    selectedItem.Width = w;
                    selectedItem.Height = h;
                }

                unsavedChanges = true;
            }

            if (panning == true)
            {
                viewBoxOffset = new FPoint(originalvbo.X - ((x - mouseStartX) * zoomFactor), originalvbo.Y - ((y - mouseStartY) * zoomFactor));
                updateViewBoxSettings();
            }

            if (mouseDown || isDrawingCurve) { previewPanel.Invalidate(); previewPanel.Update(); previewPanel.Refresh(); }
        }

        private void mainPanelMouseUp(object sender, MouseEventArgs e)
        {
            updateFormTitle();
            float x = e.X;
            float y = e.Y;
            float tx = ((float)e.X * zoomFactor) + viewBoxOffset.X;
            float ty = ((float)e.Y * zoomFactor) + viewBoxOffset.Y;

            if (e.Button == MouseButtons.Left)
            {
                switch (selectedTool)
                {
                    case (SelectedTool.None):
                        Cursor.Current = Cursors.Default;
                        if (Control.ModifierKeys != Keys.Shift) deselectAll();
                        if (reshapingObject)
                        {
                            reshapingObject = false;
                            updatePreviewPanel(false);
                            draggingPoint = -1;
                        }
                            if (preview != null && Math.Abs(preview.Width) > 10 && Math.Abs(preview.Height) > 10)
                            {
                                ((Rect)preview).Simplify();
                                selectGroup(preview.X, preview.Y, preview.Width, preview.Height);
                            }
                            else selectShape(tx, ty);
                            if (selectedItem != null) selectedItem.Simplify();
                            
                        break;


                    case (SelectedTool.Rectangle):
                        if (preview != null)
                        {
                            preview.Simplify();
                            if (preview.Width != 0 && preview.Height != 0)
                            {
                                shapes.Add(preview);
                            }
                            addToHistory();
                        }
                        break;

                    case (SelectedTool.Ellipse):
                        if (preview != null)
                        {
                            preview.Simplify();
                            if (preview.Width != 0 && preview.Height != 0)
                            {
                                shapes.Add(preview);
                            }
                            addToHistory();
                        }
                        break;

                    case (SelectedTool.Text):
                        shapes.Add(new Text(tx, ty));
                        addToHistory();
                        break;

                    case (SelectedTool.Pen):
                        FreeLine newLine = new FreeLine(mouseStartX, mouseStartY, selectedStrokeWidth, new List<FPoint>(pointList))
                        {
                            Stroke = selectedStrokeColor,
                            StrokeWidth = selectedStrokeWidth,
                            StrokeOpacity = selectedStrokeOpacity,
                            ViewBoxOffset = viewBoxOffset,
                            ZoomFactor = zoomFactor
                        };
                        shapes.Add(newLine);

                        pointList.Clear();
                        mouseStartX = -1;
                        mouseStartY = -1;
                        addToHistory();
                        break;
                }
                if (!isDrawingCurve)
                {
                    preview = null;
                    mouseDown = false;
                    resizingObject = false;
                    rotatingObject = false;
                    movingObject = false;
                    mousePreviousX = -1;
                    mousePreviousY = -1;
                    mouseStartX = -1;
                    mouseStartY = -1;
                    mouseStartTwoX = -1;
                    mouseStartTwoY = -1;

                    updateItemButtons();
                    updateSelectButtons();
                    sortShapes();
                    updateViewBoxSettings();
                    updateMainPanel();
                }

            }
            else if (e.Button == MouseButtons.Middle)
            {
                panning = false;
                mouseStartX = -1;
                mouseStartY = -1;
            }
            updatePreviewPanel(false);
        }

        private void qPressed()
        {
            if (isDrawingCurve)
            {
                preview = null;
                unsavedChanges = true;
                isDrawingCurve = false;

                Curve curve = new Curve(mouseStartX, mouseStartY, selectedStrokeWidth, new List<FPoint>(curvePointList))
                {
                    Stroke = selectedStrokeColor,
                    StrokeWidth = selectedStrokeWidth,
                    StrokeOpacity = selectedStrokeOpacity,
                    ViewBoxOffset = viewBoxOffset,
                    ZoomFactor = zoomFactor
                };
                shapes.Add(curve);

                pointList.Clear();
                curvePointList.Clear();
                mouseStartX = -1;
                mouseStartY = -1;
                addToHistory();
                preview = null;
                mouseDown = false;
                resizingObject = false;
                movingObject = false;
                mousePreviousX = -1;
                mousePreviousY = -1;
                mouseStartX = -1;
                mouseStartY = -1;
                mouseStartTwoX = -1;
                mouseStartTwoY = -1;

                updateItemButtons();
                updateSelectButtons();
                sortShapes();
                updatePreviewPanel(false);
                updateViewBoxSettings();
                updateMainPanel();
            }
        }

        private void escapePressed()
        {
            preview = null;

            isDrawingCurve = false;
            pointList.Clear();
            curvePointList.Clear();
            mouseStartX = -1;
            mouseStartY = -1;
            addToHistory();
            preview = null;
            mouseDown = false;
            resizingObject = false;
            movingObject = false;
            rotatingObject = false;
            mousePreviousX = -1;
            mousePreviousY = -1;
            mouseStartX = -1;
            mouseStartY = -1;
            mouseStartTwoX = -1;
            mouseStartTwoY = -1;

            updateItemButtons();
            updateSelectButtons();
            sortShapes();
            updatePreviewPanel(false);
            updateViewBoxSettings();
            updateMainPanel();
        }

        private void zoom(object sender, MouseEventArgs mea)
        {
            if (Control.ModifierKeys == Keys.Control)
            {
                this.zoomFactor /= (1 - mea.Delta / 480f);
                float dx, dy;
                float centeringFactor = 0;
                if (zoomFactor < 1) centeringFactor = zoomFactor; else centeringFactor = 1;

                dx = (mea.X - this.viewBoxOffset.X) * (-mea.Delta / 480f);
                dy = (mea.Y - this.viewBoxOffset.Y) * (-mea.Delta / 480f);
                dx *= centeringFactor;
                dy *= centeringFactor;
                this.viewBoxOffset = new FPoint(this.viewBoxOffset.X + dx, this.viewBoxOffset.Y + dy);
            }
            else if (Control.ModifierKeys == Keys.Shift)
            {
                this.viewBoxOffset = new FPoint(viewBoxOffset.X - mea.Delta * zoomFactor / 2, viewBoxOffset.Y);
            }
            else
            {
                this.viewBoxOffset = new FPoint(viewBoxOffset.X, viewBoxOffset.Y - mea.Delta * zoomFactor / 2);
            }
            updateViewBoxSettings();
            updateMainPanel();
        }

        private void selectShape(float x, float y)
        {
            List<Shape> possibilities = new List<Shape>();

            foreach (Shape s in shapes)
            {
                if (insideShape(x, y, s)) possibilities.Add(s);
            }

            if (possibilities.Count < 1) return;

            int highestdepth = 1000000000;
            Shape output = possibilities[0];

            foreach (Shape s in possibilities)
            {
                if (s.Depth <= highestdepth)
                {
                    output = s;
                }
            }

            if ((Control.ModifierKeys != Keys.Shift)) selectedItem = output;
            if (output.Selected == false) output.Selected = true;
            else output.Selected = false;
        }

        private void selectGroup(float x, float y, float w, float h)
        {
            ShapeGroup selectionGroup = new ShapeGroup(null, false)
            {
                Selected = true,
                ZoomFactor = zoomFactor,
                ViewBoxOffset = viewBoxOffset
            };
            int amount = 0;

            foreach (Shape s in shapes)
            {
                float maxx = Math.Max(s.X, s.X + s.Width);
                float minx = Math.Min(s.X, s.X + s.Width);
                float maxy = Math.Max(s.Y, s.Y + s.Height);
                float miny = Math.Min(s.Y, s.Y + s.Height);

                if (x < minx && x + w > maxx && y < miny && y + h > maxy)
                {
                    amount++;
                    s.Selected = true;
                    selectionGroup.AddItem(s);
                    selectedItem = s;
                }
            }

            if (amount > 1) selectedItem = selectionGroup;
        }

        private void deselectAll()
        {
            foreach (Shape s in shapes)
            {
                s.Selected = false;
            }
            selectedItem = null;
        }

        private void sortShapes()
        {
            shapes.Sort(Shape.Compare);
        }

        private void formResize(object sender, EventArgs e)
        {
            MainDrawingPanel.Size = new Size(ClientSize.Width - SideUIPanel.Width, ClientSize.Height - TopUIPanel.Height - BottomUIPanel.Height);
            previewPanel.Size = MainDrawingPanel.Size;
            previewPanel.Location = new Point(MainDrawingPanel.Location.X, MainDrawingPanel.Location.Y - 30);
            SideUIPanel.Location = new Point(ClientSize.Width - SideUIPanel.Width, 0);
            BottomUIPanel.Location = new Point(0, ClientSize.Height - BottomUIPanel.Height);
            SideUIPanelTwo.Location = new Point(ClientSize.Width - SideUIPanelTwo.Width, ClientSize.Height - SideUIPanelTwo.Height);
            updateViewBoxSettings();
            updateMainPanel();
        }

        private void saveAsSVG(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog { Filter = "Scalable Vector Graphics|*.svg" };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filename = dialog.FileName;
                saveSVG(sender, e);
            }
        }

        private void saveSVG(object sender, EventArgs e)
        {
            if (filename != null && filename != "")
            {
                //These urls are definitions wich svg version we use and how to parse it (only matters for exporting to other programs like Chrome)
                string output = $"<svg xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:cc=\"http://creativecommons.org/ns#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:svg=\"http://www.w3.org/2000/svg\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"  width=\"{MainDrawingPanel.Width}mm\" height=\"{MainDrawingPanel.Height}mm\">" + Environment.NewLine;

                foreach (Shape s in shapes)
                {
                    output += s.Export();
                    output += Environment.NewLine;
                }

                output += "</svg>";

                File.WriteAllText(filename, output);
                unsavedChanges = false;
                updateFormTitle();
            }
            else saveAsSVG(sender, e);
        }

        private void exportPNGClick(object sender, EventArgs e)
        {
            Bitmap output = new Bitmap(MainDrawingPanel.Width, MainDrawingPanel.Height);
            var gr = Graphics.FromImage(output);

            mainPanelPaint(null, new PaintEventArgs(gr, new Rectangle(0, 0, 1, 1)));

            //Our own code from mandelbrot
            var dialog = new SaveFileDialog { Filter = "All image formats|*.png;*.jpg;*.tiff;*.gif;*.ico;*.bmp;*.emf;*.wmf|PNG|*.png|JPG|*.jpg|TIFF|*.tiff|GIF|*.gif|Icon|*.ico|BMP|*.bmp|Enhanced metafile (EMF)|*.emf|Windows metafile(WMF)|*.wmf" };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string filename = dialog.FileName;
                string extension = Path.GetExtension(filename);
                ImageFormat imageFormat = ImageFormat.Bmp;

                switch (extension)
                {
                    case ".png":
                        imageFormat = ImageFormat.Png;
                        break;
                    case ".jpg":
                        imageFormat = ImageFormat.Jpeg;
                        break;
                    case ".tiff":
                        imageFormat = ImageFormat.Tiff;
                        break;
                    case ".gif":
                        imageFormat = ImageFormat.Gif;
                        break;
                    case ".ico":
                        imageFormat = ImageFormat.Icon;
                        break;
                    case ".bmp":
                        imageFormat = ImageFormat.Bmp;
                        break;
                    case ".emf":
                        imageFormat = ImageFormat.Emf;
                        break;
                    case ".wmf":
                        imageFormat = ImageFormat.Wmf;
                        break;
                }

                output.Save(dialog.FileName, imageFormat);
            }
        }

        private void updateSelectButtons()
        {
            if (selectedItem == null)
            {
                selectFillValue.BackColor = selectedFillColor;
                selectStrokeValue.BackColor = selectedStrokeColor;
                selectStrokeWidthValue.Text = selectedStrokeWidth.ToString();
                selectDepthValue.Text = selectedDepth.ToString();
                selectFillOpacityValue.Text = selectedFillOpacity.ToString();
                selectStrokeOpacityValue.Text = selectedStrokeOpacity.ToString();
            }
            else
            {
                selectFillValue.BackColor = selectedItem.Fill;
                selectStrokeValue.BackColor = selectedItem.Stroke;
                selectStrokeWidthValue.Text = selectedItem.StrokeWidth.ToString();
                selectDepthValue.Text = selectedItem.Depth.ToString();
                selectFillOpacityValue.Text = selectedItem.FillOpacity.ToString();
                selectStrokeOpacityValue.Text = selectedItem.StrokeOpacity.ToString();
            }
            invisibleButton.Focus();
            invisibleButton.Select();
        }

        private void updateToolButtons()
        {
            foreach (Control c in TopUIPanel.Controls)
            {
                c.BackColor = default(Color);
                c.ForeColor = Color.Black;
            }
            Color selectedColor = Color.Blue;
            Color selectedForeColor = Color.White;
            switch (selectedTool)
            {
                case (SelectedTool.None):
                    toolNone.BackColor = selectedColor;
                    toolNone.ForeColor = selectedForeColor;
                    break;
                case (SelectedTool.Pen):
                    toolPen.BackColor = selectedColor;
                    toolPen.ForeColor = selectedForeColor;
                    break;
                case (SelectedTool.Curve):
                    toolCurve.BackColor = selectedColor;
                    toolCurve.ForeColor = selectedForeColor;
                    break;
                case (SelectedTool.Rectangle):
                    toolRect.BackColor = selectedColor;
                    toolRect.ForeColor = selectedForeColor;
                    break;
                case (SelectedTool.Ellipse):
                    toolEllipse.BackColor = selectedColor;
                    toolEllipse.ForeColor = selectedForeColor;
                    break;
                case (SelectedTool.Text):
                    toolText.BackColor = selectedColor;
                    toolText.ForeColor = selectedForeColor;
                    break;
            }
        }

        private void updateItemButtons()
        {
            if (selectedItem == null)
            {
                itemX.Text = "";
                itemY.Text = "";
                itemWidth.Text = "";
                itemHeight.Text = "";
                itemAngle.Text = "";
                FontUIPanel.Visible = false;
            }
            else if (selectedItem.GetType() == typeof(Text))
            {
                itemX.Text = selectedItem.X.ToString();
                itemY.Text = selectedItem.Y.ToString();
                itemWidth.Text = selectedItem.Width.ToString();
                itemHeight.Text = selectedItem.Height.ToString();
                itemAngle.Text = selectedItem.Angle.ToString();
                itemText.Text = ((Text)selectedItem).Content;
                itemFontName.Text = ((Text)selectedItem).Font.Name.ToString();
                itemFontSize.Text = ((Text)selectedItem).Font.Size.ToString();
                FontUIPanel.Visible = true;
            }
            else
            {
                itemX.Text = selectedItem.X.ToString();
                itemY.Text = selectedItem.Y.ToString();
                itemWidth.Text = selectedItem.Width.ToString();
                itemHeight.Text = selectedItem.Height.ToString();
                if (selectedItem.GetType() != typeof(ShapeGroup)) itemAngle.Text = selectedItem.Angle.ToString();
                else itemAngle.Text = "";
                FontUIPanel.Visible = false;
            }
        }

        private bool insideShape(float x, float y, Shape s)
        {
            float maxx = Math.Max(s.X, s.X + s.Width);
            float minx = Math.Min(s.X, s.X + s.Width);
            float maxy = Math.Max(s.Y, s.Y + s.Height);
            float miny = Math.Min(s.Y, s.Y + s.Height);

            if (x > minx && x < maxx && y > miny && y < maxy)
            {
                return true;
            }
            return false;
        }

        private void updateViewBoxSettings()
        {
            foreach (Shape s in shapes)
            {
                s.ViewBoxOffset = viewBoxOffset;
                s.ZoomFactor = zoomFactor;
            }
            document.ViewBoxOffset = viewBoxOffset;
            document.ZoomFactor = zoomFactor;

            if (selectedItem != null)
            {
                selectedItem.ViewBoxOffset = viewBoxOffset;
                selectedItem.ZoomFactor = zoomFactor;
            }

            updateMainPanel();
        }

        private void groupSelection(object sender, EventArgs e)
        {
            ShapeGroup output = new ShapeGroup();

            for (int i = 0; i < shapes.Count; i++)
            {
                Shape s = shapes[i];
                if (s.Selected)
                {
                    output.AddItem(s);
                    shapes.Remove(s);
                    i--;
                }
            }
            addToHistory();
            shapes.Add(output);
            updateMainPanel();
        }

        private void deGroupSelection(object sender, EventArgs e)
        {
            if (selectedItem.GetType() == typeof(ShapeGroup))
            {
                shapes.AddRange(((ShapeGroup)selectedItem).DeGroup());
                shapes.Remove(selectedItem);
                updateViewBoxSettings();
                updateMainPanel();
                unsavedChanges = true;
                addToHistory();
            }
        }

        private void updatePreviewPanel(bool enable)
        {
            if (enable)
            {
                previewPanel.Enabled = true;
                previewPanel.Visible = true;

                Bitmap output = new Bitmap(MainDrawingPanel.Width, MainDrawingPanel.Height);
                var gr = Graphics.FromImage(output);
                mainPanelPaint(null, new PaintEventArgs(gr, new Rectangle(0, 0, 1, 1)));
                bufferImage = output;

                previewPanel.Invalidate();
            }
            else
            {
                previewPanel.Enabled = false;
                previewPanel.Visible = false;
            }
        }

        private void openSVGClick(object sender, EventArgs e)
        {
            if (unsavedChanges == true)
            {
                DialogResult closingDialog = askToSave();

                if (closingDialog == DialogResult.Yes)
                {
                    saveAsSVG(null, null);
                }
                else if (closingDialog == DialogResult.No)
                {
                    //Just go on
                }
                else
                {
                    return;
                }
            }

            var dialog = new OpenFileDialog { Filter = "Scalable Vector Graphics|*.svg" };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filename = dialog.FileName;
                openSVG(filename);
            }
        }

        private void openSVG(string filename)
        {
            var fs = System.IO.File.ReadAllText(filename);
            shapes = SVGParser.ParseFile(fs, Environment.NewLine.ToCharArray());
            updateViewBoxSettings();
            updateMainPanel();
            unsavedChanges = false;
            updateFormTitle();
        }

        private void clearDrawingClick(object sender, EventArgs e)
        {
            if (unsavedChanges == true)
            {
                DialogResult closingDialog = askToSave();

                if (closingDialog == DialogResult.Yes)
                {
                    saveAsSVG(null, null);
                }
                else if (closingDialog == DialogResult.No)
                {
                    //Just go on
                }
                else
                {
                    return;
                }
            }

            shapes = new List<Shape>();
            history.Clear();
            historyLevel = -1;
            addToHistory();
            unsavedChanges = false;
            filename = "";
            updateFormTitle();
            updateMainPanel();
        }


        private void close(object o, FormClosingEventArgs e)
        {
            e.Cancel = !close();
        }

        private bool close()
        {
            if (unsavedChanges == false)
            {
                Application.Exit();
            }
            else
            {
                DialogResult dialog = askToSave();

                if (dialog == DialogResult.Yes)
                {
                    saveAsSVG(null, null);
                    return false;
                }
                else if (dialog == DialogResult.No)
                {
                    unsavedChanges = false;
                    Application.Exit();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private DialogResult askToSave()
        {
            string messageBoxText = "Do you want to save changes?";
            string caption = formTitle;
            MessageBoxButtons button = MessageBoxButtons.YesNoCancel;
            MessageBoxIcon icon = MessageBoxIcon.Warning;
            return MessageBox.Show(messageBoxText, caption, button, icon);
        }

        private void updateFormTitle()
        {
            string output = formTitle;
            if (filename != "") output += " - " + Path.GetFileName(filename);
            if (unsavedChanges) output += "*";
            Text = output;
        }

        private void contextOpen(object o, CancelEventArgs ea)
        {
            context.Items.Clear();
            context.Items.Add("Copy", null, copySelection);
            context.Items.Add("Paste", null, pasteClipboard);
            context.Items.Add("Group", null, groupSelection);
            context.Items.Add("DeGroup", null, deGroupSelection);
            context.Items.Add(new ToolStripSeparator());
            context.Items.Add("Undo", null, turnBackHistory);
            context.Items.Add("Redo", null, turnBackHistoryBack);
            context.Items.Add(new ToolStripSeparator());
            context.Items.Add("Open", null, openSVGClick);
            context.Items.Add("Save", null, saveSVG);
            context.Items.Add("Save As", null, saveAsSVG);
            ea.Cancel = false;
        }

        private void addToHistory()
        {
            history.Add(shapes.Select(item => (Shape)item.Clone()).ToList());
            historyLevel = -1;
        }

        private void turnBackHistory(object sender = null, EventArgs e = null)
        {
            try
            {
                if (historyLevel == -1)
                {
                    shapes = history[history.Count - 2];
                    historyLevel = 1;
                }
                else
                {
                    shapes = history[history.Count - 2 - historyLevel];
                    historyLevel += 1;
                }
                updateMainPanel();
            }
            catch { }
        }

        private void turnBackHistoryBack(object sender = null, EventArgs e = null)
        {
            try
            {
                if (historyLevel != -1)
                {
                    shapes = history[history.Count - historyLevel];
                    historyLevel -= 1;
                }
                updateMainPanel();
            }
            catch { }
        }
    }
}
