﻿using SchetsPlusLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace SchetsPlusSVGParser
{
    public class SVGParser
    {
        public static List<Shape> ParseFile(string file, char[] newline)
        {
            List<Shape> elements = new List<Shape>();
            file = trimString(file, new char[] { '\n', '\t', '\r' });
            List<string> lines = new List<string>(splitOnElements(file));

            for (int i = 0; i < lines.Count; i++)
            {
                lines[i] = trimStringStart(lines[i], new char[] { '\n', '\t', '\r', ' ' });
            }

            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].StartsWith("<svg"))
                {
                    lines = lines.GetRange(i + 1, lines.Count - i - 1);
                }
            }

            return parseLines(lines, elements).Item1;
        }

        private static Tuple<List<Shape>, int> parseLines(List<string> lines, List<Shape> elements, string end = "</svg>", int startIndex = 0)
        {
            string allLines = "";
            foreach (string s in lines) { allLines += s + " "; };

            for (int i = startIndex; i < lines.Count; i++)
            {
                if (lines[i].StartsWith("<g"))
                {
                    ShapeGroup output = new ShapeGroup();
                    List<Shape> list = new List<Shape>();
                    var result = parseLines(lines, list, "</g>", i + 1);
                    i = result.Item2;
                    list = result.Item1;
                    foreach (Shape s in list)
                    {
                        output.AddItem(s);
                    }
                    elements.Add(output);
                }
                else if (lines[i].StartsWith(end))
                {
                    return new Tuple<List<Shape>, int>(elements, i);
                }
                else if (lines[i].StartsWith("<text"))
                {
                    Shape element = parseLine(lines[i]+lines[i+1]);
                    i++;
                    
                    if (element != null)
                    {
                        elements.Add(element);
                    }
                }
                else
                {
                    Shape element = parseLine(lines[i]);

                    if (element != null)
                    {
                        elements.Add(element);
                    }
                }
            }

            return new Tuple<List<Shape>, int>(elements, lines.Count);
        }

        private static Shape parseLine(string line)
        {
            Shape output = null;

            switch (line.Split(' ')[0])
            {
                case ("<ellipse"):
                    output = parseEllipse(line);
                    break;
                case ("<circle"):
                    output = parseEllipse(line);
                    break;
                case ("<rect"):
                    output = parseRect(line);
                    break;
                case ("<polyline"):
                    output = parsePolyline(line);
                    break;
                case ("<text"):
                    output = parseText(line);
                    break;
                case ("<path"):
                    output = parsePath(line);
                    break;
                case ("</g>"):
                case ("</svg>"):
                default:
                    break;
            }

            return output;
        }

        private static Rect parseRect(string line)
        {
            Rect output = new Rect(0, 0, 0, 0);
            string[] attributes = parseAttributes(line);

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("x"):
                            output.X = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("y"):
                            output.Y = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("width"):
                            output.Width = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("height"):
                            output.Height = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                        case ("transform"):
                            getTransform(output, attributes[i + 1]);
                            break;
                    }
                }
            }

            return output;
        }

        private static Ellipse parseCircle(string line)
        {
            Ellipse output = new Ellipse(0, 0, 0, 0);
            string[] attributes = parseAttributes(line);

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("cx"):
                            output.X = (float)Convert.ToDouble(attributes[i + 1]) - output.Width * 0.5f;
                            break;
                        case ("cy"):
                            output.Y = (float)Convert.ToDouble(attributes[i + 1]) - output.Height * 0.5f;
                            break;
                        case ("r"):
                            output.Width = 2 * (float)Convert.ToDouble(attributes[i + 1]);
                            output.Height = output.Width;
                            output.Y -= output.Width * 0.5f;
                            output.X -= output.Width * 0.5f;
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                        case ("transform"):
                            getTransform(output, attributes[i + 1]);
                            break;
                    }
                }
            }
            return output;
        }

        private static Ellipse parseEllipse(string line)
        {
            Ellipse output = new Ellipse(0, 0, 0, 0);
            string[] attributes = parseAttributes(line);

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("cx"):
                            output.X = (float)Convert.ToDouble(attributes[i + 1]) - output.Width * 0.5f;
                            break;
                        case ("cy"):
                            output.Y = (float)Convert.ToDouble(attributes[i + 1]) - output.Height * 0.5f;
                            break;
                        case ("rx"):
                            output.Width = 2 * (float)Convert.ToDouble(attributes[i + 1]);
                            output.X -= output.Width * 0.5f;
                            break;
                        case ("ry"):
                            output.Height = 2 * (float)Convert.ToDouble(attributes[i + 1]);
                            output.Y -= output.Height * 0.5f;
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                        case ("transform"):
                            getTransform(output, attributes[i + 1]);
                            break;
                    }
                }
            }
            return output;
        }

        private static FreeLine parsePolyline(string line)
        {
            FreeLine output = new FreeLine(0, 0, 1, new List<FPoint>());
            string[] attributes = parseAttributes(line,'"');

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("points"):
                            float[] points = parseStringArray(attributes[i + 1], ',', ' ');
                            List<FPoint> pointslist = new List<FPoint>();

                            for (int j = 0; j < points.Length - 1; j += 2)
                            {
                                pointslist.Add(new FPoint(points[j], points[j + 1]));
                            }

                            output.PointList = pointslist;
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                    }
                }
            }
            return output;
        }

        private static Curve parsePath(string line)
        {
            Curve output = new Curve(0, 0, 1, new List<FPoint>());
            string[] attributes = parseAttributes(line, '"');

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("d"):
                            string input = trimString(attributes[i + 1],new char[] {',','M','C' });
                            float[] points = parseStringArray(input, ' ', ' ');

                            List<FPoint> pointslist = new List<FPoint>();

                            for (int j = 0; j < points.Length - 1; j += 2)
                            {
                                pointslist.Add(new FPoint(points[j], points[j + 1]));
                            }

                            output.PointList = pointslist;
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                    }
                }
            }
            return output;
        }

        private static Text parseText(string line)
        {
            Text output = new Text(0,0);
            int ellen = line.IndexOf(">");
            output.Content = line.Substring(ellen + 1, line.Length - ellen - 8);
            line = line.Substring(0,ellen);
            string[] attributes = parseAttributes(line,'\"');

            for (int i = 0; i < attributes.Length; i += 2)
            {
                if (i + 1 < attributes.Length && attributes[i + 1] != "none")
                {
                    switch (attributes[i])
                    {
                        case ("x"):
                            output.X = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("y"):
                            output.Y = (float)Convert.ToDouble(attributes[i + 1]);
                            break;
                        case ("style"):
                            getStyle(output, attributes[i + 1]);
                            break;
                        case ("transform"):
                            getTransform(output, attributes[i + 1]);
                            break;
                    }
                }
            }
            return output;
        }

        private static Shape getStyle(Shape element, string styleLine)
        {
            string[] styles = parseAttributes(styleLine, '\0', ';', ':', 0);

            for (int i = 0; i < styles.Length; i += 2)
            {
                if (styles[i + 1] != "none")
                {
                    switch (styles[i])
                    {
                        case ("fill"):
                            element.Fill = parseColor(styles[i + 1]);
                            break;
                        case ("fill-opacity"):
                            element.FillOpacity = Convert.ToInt16(styles[i + 1]);
                            break;
                        case ("stroke"):
                            element.Stroke = parseColor(styles[i + 1]);
                            break;
                        case ("stroke-opacity"):
                            element.StrokeOpacity = Convert.ToInt16(styles[i + 1]);
                            break;
                        case ("opacity"):
                            element.FillOpacity = Convert.ToInt16(styles[i + 1]);
                            element.StrokeOpacity = Convert.ToInt16(styles[i + 1]);
                            break;
                        case ("stroke-width"):
                            element.StrokeWidth = (float)Convert.ToDouble(styles[i + 1]);
                            break;
                        case ("stroke-dasharray"):
                            element.StrokeDashes = parseStringArray(styles[i + 1]);
                            break;
                        case ("font-family"):
                            if (element.GetType() == typeof(Text)) {
                                ((Text)element).Font = new Font(styles[i + 1], ((Text)element).Font.Size);
                            }
                            break;
                        case ("font-size"):
                            if (element.GetType() == typeof(Text))
                            {
                                ((Text)element).Font = new Font(((Text)element).Font.Name.ToString(), (float)Convert.ToDouble(extractAlphaChars(styles[i+1])));
                            }
                            break;
                        case ("text-decoration"):
                            if (element.GetType() == typeof(Text))
                            {
                                var fs = new FontStyle();

                                switch (styles[i + 1])
                                {
                                    case ("underline"):
                                        fs = FontStyle.Underline;
                                        break;
                                    case ("line-through"):
                                        fs = FontStyle.Strikeout;
                                        break;
                                }

                                ((Text)element).Font = new Font(((Text)element).Font, ((Text)element).Font.Style ^ fs);
                            }
                            break;
                        case ("font-weight"):
                            if (element.GetType() == typeof(Text))
                            {
                                var fs = new FontStyle();

                                switch (styles[i + 1])
                                {
                                    case ("bold"):
                                        fs = FontStyle.Bold;
                                        break;
                                    default:
                                        fs = FontStyle.Regular;
                                        break;
                                }

                                ((Text)element).Font = new Font(((Text)element).Font, ((Text)element).Font.Style ^ fs);
                            }
                            break;
                        case ("font-style"):
                            if (element.GetType() == typeof(Text))
                            {
                                var fs = new FontStyle();

                                switch (styles[i + 1])
                                {
                                    case ("italic"):
                                    case ("oblique"):
                                        fs = FontStyle.Italic;
                                        break;
                                    default:
                                        fs = FontStyle.Regular;
                                        break;
                                }

                                ((Text)element).Font = new Font(((Text)element).Font, ((Text)element).Font.Style ^ fs);
                            }
                            break;
                        default:
                            //Error 404: 🥒 not found
                            break;
                    }
                }
            }
            return element;
        }

        private static Shape getTransform(Shape element, string line)
        {
            string[] words = line.Split();
            foreach (string w in words)
            {
                if (w.StartsWith("rotate("))
                {
                    string value = w.Substring(7, w.Length - 7);
                    element.Angle = (float)Convert.ToDouble(value);
                }
            }
            return element;
        }

        private static string[] parseAttributes(string s, char encapuslating = '\0', char delimeter = ' ', char delimeter2 = '=', int encapsulatingChars = 1)
        {
            List<string> output = new List<string>();

            while (s != "")
            {
                if (s.StartsWith("<"))
                {
                    int elementlen = s.IndexOf(delimeter);
                    if (elementlen == -1) break;
                    s = s.Substring(elementlen + 1, s.Length - elementlen - 1);
                }
                
                s = trimStringStart(s, new char[] { delimeter, delimeter2, encapuslating});
                int namelen = s.IndexOf(delimeter2);
                if (namelen == -1) break;
                string name = s.Substring(0, namelen);
                s = s.Substring(namelen + 1 + encapsulatingChars, s.Length - namelen - 1 - encapsulatingChars);
                int valuelen;
                string value;

                if (encapuslating != '\0')
                {
                    valuelen = s.IndexOf(encapuslating);
                    value = s.Substring(0, valuelen);
                    s = s.Substring(valuelen + encapsulatingChars, s.Length - valuelen - encapsulatingChars);
                }
                else
                {
                    valuelen = s.IndexOf(delimeter);
                    if (valuelen != -1) value = s.Substring(0, valuelen - encapsulatingChars);
                    else value = s.Substring(0, s.Length - encapsulatingChars);
                    s = s.Substring(valuelen + encapsulatingChars, s.Length - valuelen - encapsulatingChars);
                }

                output.Add(name);
                output.Add(value);
            }

            return output.ToArray();
        }

        private static Color parseColor(string line)
        {
            if (line.StartsWith("rgb("))
            {
                line = line.Substring(4, line.Length - 5);
                string[] values = line.Split(',');
                return Color.FromArgb(Convert.ToInt16(values[0]), Convert.ToInt16(values[1]), Convert.ToInt16(values[2]));
            }
            else if (line.StartsWith("#"))
            {
                line = line.Substring(1, line.Length - 1);
                return Color.FromArgb(Convert.ToByte(line.Substring(0, 2), 16), Convert.ToByte(line.Substring(2, 2), 16), Convert.ToByte(line.Substring(4, 2), 16));
            }

            throw new ArgumentException("Color was not correct (did not start with \"rgb(\" or \"#\"");
        }

        private static float[] parseStringArray(string line, char d1 = ',', char d2 = '\0')
        {
            List<float> output = new List<float>();

            string[] split1 = line.Split(d1);
            List<string> words = new List<string>();

            if (d2 != '\0')
            {
                foreach(string s in split1)
                {
                    words.AddRange(s.Split(d2));
                }
            } else { words = new List<string>(split1); }

            foreach (string s in words)
            {
                output.Add((float)Convert.ToDouble(s));
            }

            return output.ToArray();
        }

        private static string[] splitOnElements(string file)
        {
            List<string> output = new List<string>();
            string buffer = "";

            foreach (char c in file)
            {
                buffer += c;
                if (c == '>')
                {
                    output.Add(buffer);
                    buffer = "";
                }
            }

            return output.ToArray();
        }

        private static string[] toSingleStringArray(string[][] input)
        {
            string[] output = new string[] { };

            foreach (string[] ar in input)
            {
                output.Concat(ar);
            }

            return output;
        }

        private static string trimString(string input, char[] chars)
        {
            string output = "";
            bool match = false;

            foreach (char c in input)
            {
                match = false;
                foreach (char t in chars)
                {
                    if (c == t)
                    {
                        match = true;
                        break;
                    }
                }
                if (!match) output += c;
            }

            return output;
        }

        private static string trimStringStart(string input, char[] chars)
        {
            bool match = false;
            int i = 0;

            for (; i < input.Length; i++)
            {
                match = false;
                foreach (char t in chars)
                {
                    if (input[i] == t)
                    {
                        match = true;
                        break;
                    }
                }
                if (!match) break;
            }

            return input.Substring(i, input.Length - i);
        }

        private static string extractAlphaChars(string input)
        {
            string output = "";

            foreach (char c in input)
            {
                if (Char.IsDigit(c) || c == '.') output += c;
            }

            return output;
        }
    }
}
